/**
* @file remote.cpp
* @brief This node runs on the host computer and allows the robot to be controlled
* using the keyboard
* @author Jonathan Frahm
* @version 1.0
* @date 2015-02-07
*/
#include "boost/program_options.hpp"
#include <ros/ros.h>
#include <robot_soccer/Velocity.h>
#include <iostream>
#include <termios.h>
#include <unistd.h>
#include <signal.h>
#include <string>
#define FORWARD 'w'
#define BACKWARD 's'
#define LEFT 'a'
#define RIGHT 'd'
#define STOP 'x'
#define ROTATE_LEFT 'q'
#define ROTATE_RIGHT 'e'
#define EXIT_COMMAND 'l'

/**
* @brief Program parameters and gloabl variables 
*/
namespace {
	// Program parameters
	float LINE_SPEED = 1;
	float ANG_SPEED = 1;
	float DURATION = 0;

	// Global variables for this file
	static struct termios oldt;
	ros::Publisher channel;

	//Exit codes
	const size_t SUCCESS = 0;
	const size_t ERROR_IN_COMMAND_LINE = 1;
}


/**
* @brief Send a velocity command to the robot
*
*
* @param x Velocity in x direction
* @param y Velocity in y direction
* @param omega Rotational velocity
*/
void issueCommand(float x, float y, float omega){
	robot_soccer::Velocity v;
	robot_soccer::Velocity v2;
	v.x = x;
	v.y = y;
	v.omega = omega;
	v.theta = 0;

	v2.x = 0;
	v2.y = 0;
	v2.omega = 0;
	v2.theta = 0;
	
	channel.publish(v);
	if (DURATION){
		ros::Duration(DURATION).sleep();
		channel.publish(v2);
	}
}

/**
* @brief Interrupt handler for ctrl-c
*
* @param s Interrupt value
*/
void close_program(int s){
	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	exit(0);
}

/**
* @brief Main function for remote node
*
* @param argc Number of command line arguemnts
* @param argv Command line arguments
*
* @return Exit status
*/
int main(int argc, char **argv){
	std::string target = "robot1";
	namespace po = boost::program_options;
	po::options_description desc("Options");
	desc.add_options()
		("help,h", "Print help messages")
		("line-speed,l",po::value<float>(&LINE_SPEED),"magnitude of lateral velocity")
		("ang-speed,a",po::value<float>(&ANG_SPEED),"magnitude of angular velocity")
		("duration,d",po::value<float>(&DURATION),"time in seconds for command to persist")
		("robot-msg,r",po::value<std::string>(&target),"name of ROS topic to send velocity commands to");
	po::variables_map vm;
	try{
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);

		if (vm.count("help")){
			std::cout << "Remote control node" << std::endl
								<< desc << std::endl;
			return SUCCESS;
		}
	}
	catch(po::error& e){
		std::cerr << "ERROR: " << e.what() << std::endl << std::endl
							<< desc << std::endl;
		return ERROR_IN_COMMAND_LINE;
	}

	ros::init(argc, argv, "remote");

	ros::NodeHandle handle;

	channel = handle.advertise<robot_soccer::Velocity>(target, 5);

	ros::Rate command_rate(10);

	// Read every character directly from stdin without waiting for newline
	static struct termios newt;
	tcgetattr( STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~(ICANON);
	newt.c_lflag &= ~ECHO || ECHOCTL;
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);

	// Reset terminal to default settings on ctrl-c
	struct sigaction intHandler;
	intHandler.sa_handler = close_program;
	sigemptyset(&intHandler.sa_mask);
	intHandler.sa_flags = 0;
	sigaction(SIGINT, &intHandler, NULL);
	
	while(ros::ok()){
		char command = getchar();
		
		switch(command) {
			case FORWARD:
				issueCommand(LINE_SPEED, 0, 0);
			break;
			case BACKWARD:
				issueCommand(-LINE_SPEED, 0, 0);
			break;
			case LEFT:
				issueCommand(0, LINE_SPEED, 0);
			break;
			case RIGHT:
				issueCommand(0, -LINE_SPEED, 0);
			break;
			case ROTATE_LEFT:
				issueCommand(0, 0, ANG_SPEED);
			break;
			case ROTATE_RIGHT:
				issueCommand(0, 0, -ANG_SPEED);
			break;
			case STOP:
				issueCommand(0, 0, 0);
			break;
			case EXIT_COMMAND:
				// reset terminal settings
				tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
				return 0;
			break;
			default:
				std::cout << "Invalid command" << std::endl;
		}
		command_rate.sleep();
	}
	return 0;	
			
}
