#include <cairo.h>
#include <gtk/gtk.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include "../vision/FieldInfo.h"
#include "ros/ros.h"
#include "ros/package.h"
#include "robot_soccer/GameState.h"

namespace {
	float x_offset;
	float y_offset;
	std::string STATE_TOPIC = "simulator_state";
	float ROBOT_HALF_WIDTH = 29.5f;
	float ROBOT_HALF_HEIGHT = 34;
	float BALL_HALF_WIDTH = 7;
	float BALL_HALF_HEIGHT = 7;
	
	int robot1_x;
	int robot1_y;
	float robot2_angle;
	int robot2_x;
	int robot2_y;
	float robot1_angle;
	int ball_x;
	int ball_y;
};

struct {
	cairo_surface_t *field;
  cairo_surface_t *robot_home1;  
  cairo_surface_t *robot_away1;  
  cairo_surface_t *ball;  
} glob;

void processState(const robot_soccer::GameState::ConstPtr& state){
	robot1_angle = -1*state->objects[0].angle;
	robot1_x = (int) ((((x_offset + state->objects[0].x)*1000)/3) + 10 - ROBOT_HALF_WIDTH);
	robot1_y = (int) ((((y_offset - state->objects[0].y)*1000)/3) + 10 - ROBOT_HALF_HEIGHT);
	robot2_angle = -1*state->objects[1].angle;
	robot2_x = (int) ((((x_offset + state->objects[1].x)*1000)/3) + 10 - ROBOT_HALF_WIDTH);
	robot2_y = (int) ((((y_offset - state->objects[1].y)*1000)/3) + 10 - ROBOT_HALF_HEIGHT);
	ball_x = (int) ((((state->objects[2].x + x_offset)*1000)/3) + 10 - BALL_HALF_WIDTH);
	ball_y = (int) ((((y_offset - state->objects[2].y)*1000)/3) + 10 - BALL_HALF_HEIGHT);

}

static gboolean on_expose_event(GtkWidget *widget, GdkEventExpose *event, gpointer data){
  cairo_t *cr_field, *cr_robot1, *cr_robot2, *cr_ball;

  cr_field = gdk_cairo_create(widget->window);
  cr_robot1 = gdk_cairo_create(widget->window);
  cr_robot2 = gdk_cairo_create(widget->window);
  cr_ball = gdk_cairo_create(widget->window);
	
	cairo_set_source_surface(cr_field, glob.field, 10, 10);
  cairo_paint(cr_field);   

	cairo_translate(cr_robot1, robot1_x + ROBOT_HALF_WIDTH, robot1_y + ROBOT_HALF_HEIGHT);
	cairo_rotate(cr_robot1, robot1_angle);
	cairo_translate(cr_robot1, -robot1_x - ROBOT_HALF_WIDTH, -robot1_y - ROBOT_HALF_HEIGHT);
	cairo_set_source_surface(cr_robot1, glob.robot_home1, robot1_x, robot1_y);
  cairo_paint(cr_robot1);   

	cairo_translate(cr_robot2, robot2_x + ROBOT_HALF_WIDTH, robot2_y + ROBOT_HALF_HEIGHT);
	cairo_rotate(cr_robot2, robot2_angle);
	cairo_translate(cr_robot2, -robot2_x - ROBOT_HALF_WIDTH, -robot2_y - ROBOT_HALF_HEIGHT);
	cairo_set_source_surface(cr_robot2, glob.robot_away1, robot2_x, robot2_y);
  cairo_paint(cr_robot2);   

	cairo_set_source_surface(cr_ball, glob.ball, ball_x, ball_y);
  cairo_paint(cr_ball);   

	cairo_destroy(cr_field);
	cairo_destroy(cr_robot1);
	cairo_destroy(cr_robot2);
	cairo_destroy(cr_ball);

  return FALSE;
}

static gboolean time_handler(GtkWidget *widget){
  if (widget->window == NULL) return FALSE;
	ros::spinOnce();
	// Signals expose event
  gtk_widget_queue_draw(widget);
  return TRUE;
}

int main (int argc, char *argv[]){
	ros::init(argc, argv, "simulator_gui");
	ros::NodeHandle handle;
	
	x_offset = FieldInfo::WIDTH/2;
	y_offset = FieldInfo::HEIGHT/2;

  GtkWidget *window;
  GtkWidget *darea;
	
	std::ostringstream field_path, robot_home_path, robot_away_path, ball_path;
	field_path << ros::package::getPath("robot_soccer") << "/img/field.png";
	robot_home_path << ros::package::getPath("robot_soccer") << "/img/robot_home.png";
	robot_away_path << ros::package::getPath("robot_soccer") << "/img/robot_away.png";
	ball_path << ros::package::getPath("robot_soccer") << "/img/ball.png";

  glob.field = cairo_image_surface_create_from_png(field_path.str().c_str()); 
	glob.robot_home1 = cairo_image_surface_create_from_png(robot_home_path.str().c_str());
	glob.robot_away1 = cairo_image_surface_create_from_png(robot_away_path.str().c_str());
	glob.ball = cairo_image_surface_create_from_png(ball_path.str().c_str());

  gtk_init(&argc, &argv);

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  darea = gtk_drawing_area_new();
  gtk_container_add(GTK_CONTAINER (window), darea);

  g_signal_connect(darea, "expose-event",
      G_CALLBACK(on_expose_event), NULL);
  g_signal_connect(window, "destroy",
      G_CALLBACK(gtk_main_quit), NULL);

  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(window), 1212, 872);

  gtk_window_set_title(GTK_WINDOW(window), "Robot Soccer Simulator");
  g_timeout_add(20, (GSourceFunc) time_handler, (gpointer) window);
  gtk_widget_show_all(window);
  time_handler(window);
	ros::Subscriber subscriber = handle.subscribe(STATE_TOPIC, 1, processState);

  gtk_main();

	cairo_surface_destroy(glob.robot_home1);
	cairo_surface_destroy(glob.robot_away1);
	cairo_surface_destroy(glob.ball);
	cairo_surface_destroy(glob.field);

  return 0;
}


