/**
* @file game_object.cpp
* @brief Definitions for GameObject class
* @author Jonathan Frahm
* @version 1.0
* @date 2015-03-17
*/
#include "game_object.h"
#include "robot_soccer/GameState.h"
#include "robot_soccer/Velocity.h"
#include "robot_soccer/Object.h"
#include "field.h"
#include <Box2D/Box2D.h>
#include <vector>
#define MAX_SIZE 30

void GameObject::updateFromState(const robot_soccer::GameState::ConstPtr& msg){
	pos.x = (msg->objects[identity].x);
	pos.y = (msg->objects[identity].y);
	vel.x = (msg->objects[identity].Vx);
	vel.y = (msg->objects[identity].Vy);
	theta = (msg->objects[identity].angle);
	setPosition(pos, theta);
	setVelocity(vel);
	setOmega(0);
}

b2Vec2 GameObject::getVelocity(int timeslot) {
	int end = vel_history.size() - 1;
	if (timeslot > end)
		return vel;
	else
		return vel_history[end - timeslot];
}

void GameObject::setVelocity(b2Vec2 velocity){
	velocity *= 0.9f;
	body->SetLinearVelocity(velocity);
}

float GameObject::getOmega(int timeslot) {
	int end = omega_history.size() - 1;
	if (timeslot > end)
		return 0;
	else
		return omega_history[end - timeslot];
}

void GameObject::setOmega(float value){
	body->SetAngularVelocity(value);
}

void GameObject::setIdentity(FieldOptions::ObjectKey key){
	identity = key;
}

void GameObject::updatePositionFromVision(){
	body->SetTransform(pos, theta);
}

void GameObject::setPosition(b2Vec2 position, float angle){
	pos = position;
	theta = angle;
	body->SetTransform(pos, theta);
}

robot_soccer::Object GameObject::toROSmsg(){
	robot_soccer::Object result;
	pos = body->GetPosition();
	theta = body->GetAngle();
	theta = fmod(theta, 2.0f*b2_pi);
	theta = (theta > b2_pi) ? theta - (2 * b2_pi) : theta;
	vel = body->GetLinearVelocity();
	result.x = pos.x;
	result.y = pos.y;
	result.angle = theta;
	result.Vx = vel.x;
	result.Vy = vel.y;
	return result;
}
