/**
* @file robot_body.h
* @brief Model for robots in the simulator
* @author Jonathan Frahm
* @version 1.0
* @date 2015-03-14
*/
#include <Box2D/Box2D.h>
#include "game_object.h"
#include <ros/ros.h>
#include <string>

class RobotBody: public GameObject {
	public:
		void initialize(std::string topic, ros::NodeHandle* handle=NULL);
		void updateFromVelocity(const robot_soccer::Velocity::ConstPtr& msg); 

	private:
		b2PolygonShape hexagon;
		b2FixtureDef fixture;		
};
