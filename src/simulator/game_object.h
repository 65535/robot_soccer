/**
* @file game_object.h
* @brief Parent class for movable objects in the simulator
* @author Jonathan Frahm
* @version 1.0
* @date 2015-03-16
*/
#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include "ros/ros.h"
#include <vector>
#include <Box2D/Box2D.h>
#include "robot_soccer/GameState.h"
#include "robot_soccer/Velocity.h"
#include "robot_soccer/Object.h"
#include "field_options.h"


class GameObject {
	protected:
		std::vector<b2Vec2> vel_history;
		std::vector<float> omega_history;
		b2Vec2 pos;
		b2Vec2 vel; 
		ros::NodeHandle* handle;
		ros::Subscriber vel_subscriber;
		ros::Subscriber state_subscriber;
		FieldOptions::ObjectKey identity;
		float theta;

	public:
		void updateFromState(const robot_soccer::GameState::ConstPtr& msg); 
		b2Vec2 getVelocity(int timeslot);
		void setVelocity(b2Vec2 velocity);
		float getOmega(int timeslot);
		void setOmega(float value);
		void setIdentity(FieldOptions::ObjectKey key);
		void setPosition(b2Vec2 position, float theta);
		robot_soccer::Object toROSmsg();
		void updatePositionFromVision();
		
		b2Body* body;
};

#endif

