/**
* @file field.cpp
* @brief Definitions for Field class
* @author Jonathan Frahm
* @version 1.0
* @date 2015-03-17
*/
#include "field.h"
#include "../vision/FieldInfo.h"
#include "../shared/time_utils.h"
#include <math.h>
#define UNKNOWN -1337.0f
Field::Field(ros::NodeHandle* handle, b2Vec2 gravity) : world(gravity) {
	this->handle = handle;	
	publisher = handle->advertise<robot_soccer::GameState>("simulator_state",1);
  // Initialize the walls of the field
  initializeField();

	// For now, just initialize one robot and the ball
  robot_body_def.type = b2_dynamicBody;
  robot_body_def.active = true;
  robot_body_def.linearDamping = 1;
  robot_body_def.angularDamping = 1;
  robot_body_def.position.Set(-0.5f, 0.0f);
  home_team.body = world.CreateBody(&robot_body_def);
  home_team.setIdentity(FieldOptions::US_0);
	home_team.initialize("robot1", handle);

	// For now, just initialize one robot and the ball
	unknowns[0].type = b2_dynamicBody;
	unknowns[0].active = true;
	unknowns[0].linearDamping = 1;
	unknowns[0].angularDamping = 1;
	unknowns[0].position.Set(0.5f , 0);
  away_team.body = world.CreateBody(&(unknowns[0]));
  away_team.setIdentity(FieldOptions::OPP_0);
	away_team.initialize("fake_topic", NULL);

  ball_body_def.type = b2_dynamicBody;
  ball_body_def.active = true;
  ball_body_def.linearDamping = 0.5f;
  ball_body_def.angularDamping = 1;
  ball_body_def.position.Set(0, 0);
  ball.body = world.CreateBody(&ball_body_def);
  ball.setIdentity(FieldOptions::BALL);
	ball.initialize();
}

void Field::initializeField(){

  // Define the walls as static bodies
  b2BodyDef bottom_wall_def, top_wall_def, left_wall_def, right_wall_def;
  bottom_wall_def.type = b2_staticBody;
  top_wall_def.type = b2_staticBody;
  left_wall_def.type = b2_staticBody;
  right_wall_def.type = b2_staticBody;

  // Set the wall positions
  bottom_wall_def.position.Set(0.0f, -1*(FieldInfo::HEIGHT/2 + 0.1));
  top_wall_def.position.Set(0.0f, FieldInfo::HEIGHT/2 + 0.1);
  left_wall_def.position.Set(-1*(FieldInfo::WIDTH/2 + 0.1), 0.0f);
  right_wall_def.position.Set(FieldInfo::WIDTH/2 + 0.1, 0.0f);

  // Create the walls in the world
  b2Body* bottom_wall_body = world.CreateBody(&bottom_wall_def);
  b2Body* top_wall_body = world.CreateBody(&top_wall_def);
  b2Body* left_wall_body = world.CreateBody(&left_wall_def);
  b2Body* right_wall_body = world.CreateBody(&right_wall_def);

  // Define thin boxes for the wall shapes
  b2PolygonShape bottom_wall_box, top_wall_box, left_wall_box, right_wall_box;
  bottom_wall_box.SetAsBox(FieldInfo::WIDTH/2, 0.1);
  top_wall_box.SetAsBox(FieldInfo::WIDTH/2, 0.1);
  left_wall_box.SetAsBox(0.1, FieldInfo::HEIGHT/2);
  right_wall_box.SetAsBox(0.1, FieldInfo::HEIGHT/2);

  // Apply the shape to the walls
  bottom_wall_body->CreateFixture(&bottom_wall_box, 0.0f);
  top_wall_body->CreateFixture(&top_wall_box, 0.0f);
  left_wall_body->CreateFixture(&left_wall_box, 0.0f);
  right_wall_body->CreateFixture(&right_wall_box, 0.0f);

}

void Field::listenToState(char * vision_state){
	subscriber = handle->subscribe(vision_state, 1, &Field::updateFromState, this);
}

void Field::updateFromState(const robot_soccer::GameState::ConstPtr& msg){
	static unsigned int last_update_time = 0;
	static unsigned int timestep = 17;
	unsigned int image_time = msg->timestamp;
	unsigned int time_diff = image_time - last_update_time;
	if (last_update_time){
		last_update_time = image_time; 
		// 16.7 milliseconds per iteration of world steps
		unsigned int loop_iterations = (unsigned int) ((float) time_diff / (100.0/timestep));
		home_team.updateFromState(msg);
		away_team.updateFromState(msg);
		ball.updateFromState(msg);
		for (int i = loop_iterations; i > 0; i--){
			unsigned int regained = 0;
			while(regained < 100){
				updateWorld(i, false);
				regained += timestep;
			}
		}
	} else {
		last_update_time = image_time; 
	}
}

void Field::updatePosition(FieldOptions::ObjectKey key, b2Vec2 pos, float theta){
	switch (key){
		case FieldOptions::US_0:
			home_team.setPosition(pos, theta);
			break;
		case FieldOptions::OPP_0:
			away_team.setPosition(pos, theta);
			break;
		case FieldOptions::BALL:
			ball.setPosition(pos, theta);
			break;
	}
}

void Field::simulate(){
	while(ros::ok()){
		ros::spinOnce();
		updateWorld(0, true);
	}
}

void Field::compensate(){
	ros::Subscriber vel_subscriber = handle->subscribe("state", 1, &Field::updateFromState, this);
	simulate();
}

void Field::updateWorld(int timeslot, bool publish){
		static float32 timeStep = 1.0f / 60.0f;
		static int32 velocityIterations = 6;
		static int32 positionIterations = 2;

		static ros::Rate loop_rate(60);
		static unsigned int loop_counter = 0;
	
		home_team.setVelocity(home_team.getVelocity(timeslot));
		home_team.setOmega(home_team.getOmega(timeslot));
		world.Step(timeStep, velocityIterations, positionIterations);

		if(!publish)
			return;
		// Publish 10 times per second
		if ((++loop_counter % 60) % 6 ){
				loop_rate.sleep();
			return;
		}
		robot_soccer::GameState state;
		state.objects.push_back(home_team.toROSmsg());
		state.objects.push_back(away_team.toROSmsg());
		state.objects.push_back(ball.toROSmsg());
	
		publisher.publish(state);
		loop_rate.sleep();
}
