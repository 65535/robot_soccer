/**
* @file ball.h
* @brief Model for the golf ball
* @author Jonathan Frahm
* @version 1.0
* @date 2015-03-14
*/
#include <Box2D/Box2D.h>
#include "game_object.h"

class Ball : public GameObject {
	public:
		void initialize();
	
	private:
		b2CircleShape circle;
		b2FixtureDef fixture;		
};
