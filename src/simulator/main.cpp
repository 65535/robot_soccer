#include "boost/program_options.hpp"
#include "ros/ros.h"
#include "field.h"

namespace {
	const int SUCCESS = 0;
	const int ERROR_IN_COMMAND_LINE = 1;
}

int main(int argc, char** argv){
	ros::init(argc, argv, "simulator");
	ros::NodeHandle handle;
	
	Field field(&handle);
	char * vision_state = "state";

  namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()
    ("help,h", "Print help messages")
    ("ignore-vision,i", "Ignore updates from vision node");

  po::variables_map vm; 
  try{
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")){
      std::cout << "Simulator node -- uses Box2D" << std::endl
								<< "Usage: rosrun robot_soccer simulator [options]" << std::endl
                << desc << std::endl;
      return SUCCESS;
    } 
		if (vm.count("ignore-vision")){
			field.simulate();	
		} else {
			field.listenToState(vision_state);
			field.compensate();
		}

  }
  catch(po::error& e){ 
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl
              << desc << std::endl;
    return ERROR_IN_COMMAND_LINE;
  }
}
