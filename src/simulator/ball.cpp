#include "ball.h"

#define RADIUS 0.0427f
#define BALL_DENSITY 0.1f
#define BALL_FRICTION 0.1f

void Ball::initialize(){
	circle.m_p.Set(0.0f, 0.0f);
	circle.m_radius = RADIUS;

	fixture.shape = &circle;
	fixture.density = BALL_DENSITY;
	fixture.friction = BALL_FRICTION;
	// Make ball collisions bouncy
	fixture.restitution = 0.4;

	body->CreateFixture(&fixture);
}

