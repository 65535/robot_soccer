/**
* @file field_options.h
* @brief Variables shared by multiple classes in this directory
* @author Jonathan Frahm
* @version 1.0
* @date 2015-03-18
*/
#ifndef FIELD_OPTIONS_H
#define FIELD_OPTIONS_H

namespace FieldOptions {
	const int NUM_ROBOTS = 2;
	enum ObjectKey{
		US_0, OPP_0, BALL
	};
};
using namespace FieldOptions;
#endif

