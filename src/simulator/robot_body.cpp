#include "robot_body.h"

#define VERTEX 0.08799
#define RADIUS 0.1016
#define ROBOT_DENSITY 1.0f
#define ROBOT_FRICTION 0.0f

void RobotBody::initialize(std::string topic, ros::NodeHandle* handle){
	b2Vec2 verts[6];
	verts[0] = b2Vec2(RADIUS, 0);
	verts[1] = b2Vec2(RADIUS/2, -VERTEX);
	verts[2] = b2Vec2(-RADIUS/2, -VERTEX);
	verts[3] = b2Vec2(-RADIUS, 0);
	verts[4] = b2Vec2(-RADIUS/2, VERTEX);
	verts[5] = b2Vec2(RADIUS/2, VERTEX);

	hexagon.Set(verts, 6);

	fixture.shape = &hexagon;
	fixture.density = ROBOT_DENSITY;
	fixture.friction = ROBOT_FRICTION;
	fixture.restitution = 0.1;

	body->CreateFixture(&fixture);
	if(handle != NULL){
		vel_subscriber = handle->subscribe(topic, 5, &RobotBody::updateFromVelocity, this);
	}
}

void RobotBody::updateFromVelocity(const robot_soccer::Velocity::ConstPtr& msg){
	b2Vec2 vel_temp(msg->x, msg->y);
	vel_history.push_back(vel_temp);
	omega_history.push_back(msg->omega);
	if (vel_history.size() > 50){
		vel_history.erase(vel_history.begin());
		omega_history.erase(omega_history.begin());
	}
}

