/**
* @file field.h
* @brief Model for field with objects
* @author Jonathan Frahm
* @version 1.0
* @date 2015-03-16
*/
#include "ros/ros.h"
#include "field_options.h"
#include "robot_body.h"
#include "ball.h"
#include <Box2D/Box2D.h>

class Field {
	public:
		Field(ros::NodeHandle* handle, b2Vec2 gravity = b2Vec2(0.0f, 0.0f));
		void updatePosition(FieldOptions::ObjectKey key, b2Vec2 pos, float theta);
		void updateFromState(const robot_soccer::GameState::ConstPtr& msg);
		void simulate();
		void compensate();
		void listenToState(char * state_topic);

	private:
		void initializeField();
		void updateWorld(int timeslot, bool publish);
		void catchUp();

		b2World world;
		RobotBody home_team;
		RobotBody away_team;
		Ball ball;
		ros::NodeHandle* handle;
		ros::Publisher publisher;
		ros::Subscriber subscriber;
		b2BodyDef robot_body_def;
		b2BodyDef ball_body_def;
		b2BodyDef unknowns[3];
	  bool behind;
			
};

