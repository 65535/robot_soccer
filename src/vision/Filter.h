#ifndef FILTER_H
#define FILTER_H

#include "Vector2.h"

#include <opencv2/opencv.hpp>

namespace {
  // template weirdness to get the right Mat type based on the Filter's type
  template<typename U>
  struct MatType {
    static const int type = CV_64F;
  };

  template<>
  struct MatType<float> {
    static const int type = CV_32F;
  };
}


// TODO: could add support for control

//template<typename T>
/**
 * A class that filters and predicts object position, velocity,
 * orientation, and angular velocity.
 */
class Filter {
public:
  typedef double T;

  /** Which variables to track.
   * Tracking a certain level means tracking all levels beneath it too.
   * The values are the numbers of variables tracked/measured.
   */
  enum Level {
    POSITION    = 2,
    VELOCITY    = 4,
    ORIENTATION = 5,
    ANGULAR_VEL = 6,
  };

  enum Variable {
    PosX  = 0,
    PosY  = 1,
    VelX  = 2,
    VelY  = 3,
    Angle = 4,
    AngularVel = 5,
  };

  /**
   * Construct a new filter.
   * A tracking and measuring level is specified. All "lower" levels are included.
   *
   * @param to_track which variables to track
   * @param to_measure which variables to measure
   */
  Filter(Level to_track, Level to_measure, T process_noise_cov, T measurement_noise_cov, T error_cov);

  void setAllCovariances(T process, T measurement, T error);
  void setCovariance(Variable var, T process, T measurement, T error);

  void setInitialConditions(const Vector2<T>& init_pos = {0, 0}, const Vector2<T>& init_vel = {0, 0}, T init_orient = 0, T init_angular_velocity = 0);

  /**
   * Set how much time has passed since the last predict and correct calls.
   * This should be called once per calling the two functions predict and correct.
   * @param dt how much time has passed since the last sample
   */
  void setTimeDelta(T dt);

  /**
   * Predict the state after the set time delta.
   */
  void predict() { state_ = filter_.predict(); }

  /**
   * Correct the state with measurements.
   */
  void correct(const Vector2<T>& pos);
  void correct(const Vector2<T>& pos, const Vector2<T>& vel);
  void correct(const Vector2<T>& pos, const Vector2<T>& vel, T orientation);
  void correct(const Vector2<T>& pos, const Vector2<T>& vel, T orientation, T angular_vel);

  /** Get the current position. */
  Vector2<T> position() const { return Vector2<T>(state_.at<T>(0), state_.at<T>(1)); }

  /** Get the current velocity. */
  Vector2<T> velocity() const { return Vector2<T>(state_.at<T>(2), state_.at<T>(3)); }

  /** Get the current orientation. */
  T orientation() const { return state_.at<T>(4); }

  /** Get the current angular velocity. */
  T angularVelocity() const { return state_.at<T>(5); }

  T value(Variable var) const { return state_.at<T>(var); }

private:
  static const int MAT_TYPE = MatType<T>::type;

  Level track_, measure_;

  cv::KalmanFilter filter_;

  cv::Mat measurement_;
  cv::Mat state_;
};

#endif
