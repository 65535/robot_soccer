#ifndef FIELDINFO_H
#define FIELDINFO_H

#include "Vector2.h"

/**
 * @brief Basic information about the field.
 *
 * These values are the fixed dimensions of the field in meters.
 * TODO: get the actual values
 * TODO: Should these values change based on side (home/away)
 *       or should the Field object translate everything so it looks home? (I think the latter is better)
 */
namespace FieldInfo {
  // TODO: GET ACTUAL DIMENSIONS
  /** The width of the field */
  const float WIDTH  = 3.5560f;
  /** The height of the field */
  const float HEIGHT = 2.5400f;
  /** The height of a goal (distance between posts) */
  const float GOAL_HEIGHT = 0.6096f;

  /** Robot info */
  const float ROBOT_RADIUS = 0.2032f;

  /** The top left corner of the field */
  const Point TOP_LEFT(-WIDTH/2,  HEIGHT/2);
  /** The top right corner of the field */
  const Point TOP_RIGHT( WIDTH/2,  HEIGHT/2);
  /** The bottom left corner of the field */
  const Point BOT_LEFT(-WIDTH/2, -HEIGHT/2);
  /** The bottom right corner of the field */
  const Point BOT_RIGHT( WIDTH/2, -HEIGHT/2);

  /** The center of the field */
  const Point CENTER(0, 0);

  /** The y coordinate of the top of the field */
  const float TOP    = HEIGHT/2;
  /** The y coordinate of the bottom of the field */
  const float BOTTOM = -TOP;
  const float BOT = BOTTOM;
  /** The x coordinate of the right of the field */
  const float RIGHT  = WIDTH/2;
  /** The x coordinate of the left of the field */
  const float LEFT   = -RIGHT;

  /** The point in the center of our goal */
  const Point OUR_GOAL_CENTER(LEFT, 0.0f);
  /** The point at the top of our goal */
  const Point OUR_GOAL_TOP(LEFT, GOAL_HEIGHT/2);
  /** The point at the bottom of our goal */
  const Point OUR_GOAL_BOT(LEFT, -GOAL_HEIGHT/2);

  /** The point at the center of the opponent's goal */
  const Point GOAL_CENTER(RIGHT, 0.0f);
  /** The point at the top of the opponent's goal */
  const Point GOAL_TOP(RIGHT, GOAL_HEIGHT/2);
  /** The point at the bottom of the opponent's goal */
  const Point GOAL_BOT(RIGHT, -GOAL_HEIGHT/2);

  /** A type defining the two teams */
  enum Team {
    Away = -1, // These values must be -1 and 1 -- used by Field to swap coordinates
    Home = 1,
  };
}

#endif
