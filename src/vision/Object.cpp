#include "Object.h"

#include <iostream>

/** Initialize to "unknown" */
Object::Object() : pos(unknown<float>(), unknown<float>()), orientation(unknown<float>()), vel(unknown<float>(), unknown<float>()) {};

/** Initialize from pixel values and translate using the provided field */
Object::Object(Point position, float angle, float vx, float vy)
    : pos(position), orientation(angle), vel(vx, vy) {}

Object::Object(const Point& position, const Vector& velocity, float orientation)
    : pos(position), orientation(orientation), vel(velocity) {}

Object::Object(float x, float y, float angle, float vx, float vy)
    : pos(x, y), orientation(angle), vel(vx, vy) {}

Object::Object(const robot_soccer::Object& object)
    : pos(object.x, object.y), orientation(object.angle), vel(object.Vx, object.Vy) {}

void Object::setVelocity(const Object& prev, float delta_time_s) {
  if (isUnknown<float>(prev.pos.x) || isUnknown<float>(pos.x)) {
    vel = Vector(0.0f, 0.0f);
  }
  else {
    vel = (pos - prev.pos) / delta_time_s;
  }
}

robot_soccer::Object Object::toROS() const {
  robot_soccer::Object object;
  object.x = pos.x;
  object.y = pos.y;
  object.angle = orientation;
  object.Vx = vel.x;
  object.Vy = vel.y;

  return object;
}

Object& Object::operator=(const robot_soccer::Object& object) {
  pos.x = object.x;
  pos.y = object.y;
  orientation = object.angle;
  vel.x = object.Vx;
  vel.y = object.Vy;

  return *this;
}

std::ostream& operator<<(std::ostream& os, const Object& obj) {
  os << "Position: " << obj.pos << std::endl;
  os << "Orientation: " << obj.orientation << std::endl;
  os << "Velocity: " << obj.vel << std::endl;
  return os;
}
