#ifndef TRACKER_H
#define TRACKER_H

#include <chrono>
#include <opencv2/opencv.hpp>

#include <semaphore.h>

#include "Filter.h"
#include "GameState.h"
#include "ImageProcessor.h"
#include "Calibrator.h"

using cv::Mat;

/**
 * A Tracker tracks the ball and robots.
 *
 * The tracking is based on the color filters defined in ./calibration/filter_config.txt.
 * TODO: add Kalman filtering to this
 */
class Tracker {
  typedef Filter::T filter_t;

  constexpr static const Object::Type TO_TRACK[] {
    Object::Robot1,
    //Object::Robot2,
    Object::Opponent1,
    //Object::Opponent2,
  };

  static const int NUM_ROBOTS = 1;

  FieldInfo::Team side_;

  ImageProcessor processor_;

  // if a calibration GUI is in use, this will point to it; otherwise, it is nullptr
  Calibrator* cal;
  
  const GameState& (Tracker::*updateFunction_)(const Mat& frame);

  // TODO: THIS is very temporary
  GameState prev_state_, state_;

  // TODO: a separate Kalman Filter for each object?
  std::vector<Filter> filters;

  // variables used while processing images
  Mat images_[Object::COUNT];
  Mat hsv_;
  volatile filter_t delta_t;

  sem_t start_[Object::COUNT-1];
  sem_t done_;

  /** Initialize the filter(s) */
  void initializeFilter();

  /** Convert the angle to the range [-PI, PI) */
  template<typename T>
  static T modAngle(T angle) {
    return fmod(angle, 2*M_PI) - M_PI;
  }

  /** Update an object's when the position is unknown */
  void filter(Object::Type object);

  /** Update an object's when the position is known (and optionally heading) */
  void filter(Object::Type object, const Point& position, filter_t heading = unknown<filter_t>());

  /** Calculate the angle between two points */
  template<typename U, typename T = filter_t>
  static T findAngle(Vector2<U> p0, Vector2<U> p1) {
    T dx = p1.x - p0.x;
    T dy = p0.y - p1.y; // This inverts the y axis
    
    return atan2(dy, dx);
  }

  /**
   * Get the heading of the object.
   * This factors in the side (home or away)
   */
  inline float heading(const Pixel& big, const Pixel& small) const {
    return (side_ == FieldInfo::Home) ? findAngle(big, small) : findAngle(small, big);
  }

  /** Launch a thread on the specified tracker object */
  // TODO: could a member function pointer be used instead of this?
  static void threadLauncher(Tracker* tracker, Object::Type type);

  /** start the threads for each robot */
  void startProcessing();

  /** Wait until the threads indicate that they are done, then clear their done flags */
  void waitUntilDone();

  /** Start the threads */
  void initializeThreads();

  /** Run this in a separate thread to look for object with each new image */
  void objectFinder(Object::Type object);

  template<typename T>
  inline static T timeSinceLastCall() {
    using namespace std::chrono;

    static high_resolution_clock::time_point prev_time = high_resolution_clock::now();

    high_resolution_clock::time_point time = high_resolution_clock::now();
    T delta = duration_cast<duration<T>>(time - prev_time).count();
    prev_time = time;

    return delta;
  }

  /** Update the game state with multiple threads */
  const GameState& updateMulti(const Mat& frame);

  /** Update the game state with only one thread */
  const GameState& updateSingle(const Mat& frame);

public:
  /**
   * Create a new game state tracker.
   * @param side Which side the team is on (FieldInfo::Team Home or Away)
   */
  Tracker(FieldInfo::Team side, const Mat& sample, bool multithread, bool use_calibration_gui);

  /** Destructor. */
  ~Tracker();

  /**
   * Locate the ball on the field.
   * @param pixel the pixel object to store where the ball was found
   */
  void locateBall();

  /**
   * Update the game state with a new frame.
   *
   * @return the game state based on the new frame
   */
  inline const GameState& update(const Mat& frame) {
    return (this->*updateFunction_)(frame);
  }

  /**
   * Locate a robot's position and find its orientation.
   *
   * Look for two colored blobs and check the angle between them.
   * The big blob is the back, the small the front.
   * @param threshold the binary image to work from
   * @return the robot's Object
   */
  void locateRobot(Object::Type robot);

  //TODO: These shouldn't be separate functions! Fix this! This version will die.
  Pixel locateObject(const Mat& threshold_orig, const ColorFilter& filter, unsigned num_objects = 2);

  //
  // Template function definitions
  //

  /** Find a set of objects.
   *
   * @param threshold_orig the processed threshold image to work from
   * @param filter the filter for the current object (used for blob size filtering)
   * @param max_num_objects how many objects can be found before giving up b/c too much noise
   * @param processPixel a function that processes objects when located;
   *            takes the blob area (double) and x and y pixel coordinates (int);
   *            returns true if all is well
   * @return true if successful (at least partially for robots), false if failed
   */
  template<typename F>
  bool locateObjects(const Mat& threshold_orig, const ColorFilter& filter, F processPixel, unsigned max_num_objects = 20) {
    using namespace cv;

    Mat threshold;
    threshold_orig.copyTo(threshold);

    std::vector< std::vector<Point2i> > contours;
    std::vector<Vec4i> hierarchy;

    findContours(threshold, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);

    bool success = false;

    if (hierarchy.size() > 0 && hierarchy.size() <= max_num_objects) {
      for (int index = 0; index >= 0; index = hierarchy[index][0]) {
        //std::cerr << "Found " << hierarchy.size() << " objects!" << std::endl;
        Moments moment = moments(static_cast<Mat>(contours[index]));

        if (moment.m00 >= filter.min_size && moment.m00 <= filter.max_size) {
          int x = moment.m10/moment.m00 + 0.5; // round to nearest integer
          int y = moment.m01/moment.m00 + 0.5;

          success = processPixel(moment.m00, x, y);

          if (!success) return false; // if processPixel indicated a problem, stop
        }
      }
    }
    else {
      // too noisy
      //std::cerr << "Failed to find object: too many (or too few) of them!" << std::endl;
      return false;
    }

    return success; // can only be true if processPixel ran at least once
  }
};

#endif
