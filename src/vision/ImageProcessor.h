#ifndef IMAGEPROCESSOR_H
#define IMAGEPROCESSOR_H

#include <opencv2/opencv.hpp>

#include "Object.h"
#include "ColorFilter.h"
#include "Field.h"

using cv::Mat;

class ImageProcessor {
private:
  constexpr static const char* CAL_DIR = "./calibration/";
  constexpr static const char* FILTERS_DIR = "./calibration/filters/";
  constexpr static const char* FILTER_CONFIG = "./calibration/filter_config.txt";
  constexpr static const char* FIELD_CONFIG = "./calibration/field.txt";
  constexpr static const char* CAMERA_INFO = "./calibration/camera.yml";

  constexpr static const double Y_SHIFT = 30.0;
  constexpr static const double X_SHIFT = 50.0;

  static const int NUM_ROBOTS = 2;

  // the size of the undistorted image
  cv::Size undistorted_size_;

  // the color filters
  ColorFilter filters_[2*NUM_ROBOTS + 1]; // two teams plus the ball

  // the size of the Gaussian blur to apply
  cv::Size blur_size_;

  // remap to account for camera distortion
  // map1_ contains the point map
  // map2_ contains indices in a table of interpolation coefficients (I think)
  // only map1_ is needed to remap points (map2_ just adds interpolation)
  Mat map1_, map2_;
  Mat map1_inv_;

  // the corners of the field
  Field field_;

  // which cv color conversion to use
  int color_conversion_;

  /** Read in the filter calibration data. */
  void readFilterConfig();

  /** Save the filter config file. */
  void saveFilterConfig() const;

  /** Load the camera undistortion information. */
  void loadCameraProfile(const Mat& sample);

  /** Create the map for undistorting single points */
  void createInverseMap(const Mat& sample);

public:
  /**
   * @brief Construct a new image processor.
   * This reads in the filter configuration data from disk.
   *
   * @param side which team (Home or Away)
   */
  ImageProcessor(FieldInfo::Team side, const Mat& sample, int color_conversion = cv::COLOR_BGR2HSV);

  Mat undistort(const Mat& frame) const;

  /**
   * Crop the image to the field.
   *
   * @param frame original image
   * @return the cropped image
   */
  Mat crop(const Mat& frame) const;

  /**
   * @brief Prepare an RGB frame for filtering.
   * 
   * This crops the image, blurs it, and converts it to HSV.
   *
   * @return an HSV image ready for filtering
   */
  Mat preprocess(const Mat& frame) const;

  /**
   * Filter the image for an object.
   *
   * @param hsv the HSV image to filter
   * @param object which object to filter for
   * @return the filtered image
   */
  Mat filterObject(const Mat& hsv, Object::Type object) const;

  /**
   * Filter the image for a robot.
   *
   * @param hsv the HSV image to filter
   * @param robot which robot to filter for
   * @return the filtered image
   */
  Mat filterRobot(const Mat& hsv, Object::Type robot) const;

  /**
   * Filter the image for the ball.
   *
   * @param hsv the HSV image to filter
   * @return the filtered image
   */
  Mat filterBall(const Mat& hsv) const;

  /** Get the position according to the field dimensions. */
  template<typename... Args>
  inline Point toPosition(Args... args) const {
    return field_.toPosition(args...);
  }

  /** 
   * Find the string representation of an OpenCV type.
   * I found this on Stack Overflow or something (thank you unnamed benefactor).
   */
  static std::string type2str(int type) {
    std::string r;

    unsigned char depth = type & CV_MAT_DEPTH_MASK;
    unsigned char chans = 1 + (type >> CV_CN_SHIFT);

    switch ( depth ) {
      case CV_8U:  r = "8U"; break;
      case CV_8S:  r = "8S"; break;
      case CV_16U: r = "16U"; break;
      case CV_16S: r = "16S"; break;
      case CV_32S: r = "32S"; break;
      case CV_32F: r = "32F"; break;
      case CV_64F: r = "64F"; break;
      default:     r = "User"; break;
    }

    r += "C";
    r += (chans+'0');

    return r;
  }

  /**
   * Perform an inefficient remap() without interpolation.
   * This was written to figure out how to remap individual points,
   * and should probably just be deleted now...
   */
  Mat manualRemap(Mat frame) const;

  /**
   * Apply the same operation as undistort(Mat) on a single pixel.
   * @param pixel a pixel from the original image
   * @return the location of the pixel in the undistorted image
   */
  Pixel undistortPixel(Pixel pixel) const;

  /** Convert a pixel from the full image to a pixel on the cropped image. */
  template<typename... Args>
  inline Pixel toCroppedCoordinates(Args... args) const {
    return field_.toCroppedCoordinates(args...);
  }

  /** Convert a pixel from the cropped image to a pixel on the full image. */
  template<typename... Args>
  inline Pixel toFullCoordinates(Args... args) const {
    return field_.toFullCoordinates(args...);
  }

  /** Get the pixel from the real-world position. */
  template<typename... Args>
  inline Pixel toPixel(Args... args) const {
    return field_.toPixel(args...);
  }

  /**
   * Save the specified filter to disk.
   * @param file the name of the filter file, or blank to use the original source file
   */
  void saveFilter(Object::Type object, const std::string& file = {});

  /**
   * Save the field corners to disk and update the current corners.
   * @param corners an array of the four new corners
   * @return true if the save was successful (the corners were valid)
   */
  bool saveField(const Pixel corners[4]);

  /**
   * Save the current blur setting.
   */
  inline void saveBlur() const { saveFilterConfig(); }

  //
  // Getters and setters
  //

  /** Get a mutable reference to an object filter. */
  ColorFilter& objectFilter(Object::Type object) { return filters_[object]; }

  /** Set the size of the Gaussian blur being applied. */
  void setBlurSize(int size) { blur_size_ = cv::Size(size, size); }

  /** Get the size of the Gaussian blur being applied. */
  int blurSize() { return blur_size_.width; }

  /** Get the side the team is playing as. */
  FieldInfo::Team side() const { return field_.side(); }
};

#endif
