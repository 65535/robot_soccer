// This is based on
// http://opencvexamples.blogspot.com/2014/01/kalman-filter-implementation-tracking.html#.VO5ocDVVKlM

#include <opencv2/opencv.hpp>

#include <vector>
#include <cmath>

namespace wh {
#include "Vector2.h"
}

#define drawCross( center, color, d )                                 \
line( img, Point2i( center.x - d, center.y - d ), Point2i( center.x + d, center.y + d ), color, 2, LINE_AA, 0); \
line( img, Point2i( center.x + d, center.y - d ), Point2i( center.x - d, center.y + d ), color, 2, LINE_AA, 0 )
 
using namespace cv;
using namespace std;

namespace {
  const char* WINDOW = "Kalman Time!";
  const char* TRACKBARS = "Adjust the filter!";
  Mat img(768, 1024, CV_8UC3);
  wh::Pixel mousePos(0, 0);

  vector<Point2i> mousev,kalmanv;

  void updateMousePos(int event, int x, int y, int flags, void* userdata) {
    if (event == EVENT_MOUSEMOVE) {
      mousePos.x = x;
      mousePos.y = y;
    }
    else {
      mousev.clear();
      kalmanv.clear();
      img = Scalar(0, 0, 0);
    }
  }

  KalmanFilter KF(4, 2, 0);

  const int MANTISSA_OFFSET = 100;
  const int EXPONENT_OFFSET = 10;

  template<typename T = float>
  inline T toFloat(int mantissa, int exponent) {
    const T base = 10;

    T value = static_cast<T>(mantissa - MANTISSA_OFFSET) * pow(base, static_cast<T>(exponent - EXPONENT_OFFSET));
    cout << value << endl;
    return value;
  }

  int processNoiseCov_mantissa = 1 + MANTISSA_OFFSET;
  int processNoiseCov_exponent = -5 + EXPONENT_OFFSET;
  int measurementNoiseCov_mantissa = 1 + MANTISSA_OFFSET;
  int measurementNoiseCov_exponent = -1 + EXPONENT_OFFSET;
  int errorCovPost_mantissa = 1 + MANTISSA_OFFSET;
  int errorCovPost_exponent = 0 + EXPONENT_OFFSET;

  void updateFilter(int = {}, void* = {}) {
    cout << "========================================" << endl;
    cout << "Process noise: ";
    setIdentity(KF.processNoiseCov, Scalar::all(toFloat(processNoiseCov_mantissa, processNoiseCov_exponent)));
    cout << "Measurement noise: ";
    setIdentity(KF.measurementNoiseCov, Scalar::all(toFloat(measurementNoiseCov_mantissa, measurementNoiseCov_exponent)));
    cout << "ErrorCovPost: ";
    setIdentity(KF.errorCovPost, Scalar::all(toFloat(errorCovPost_mantissa, errorCovPost_exponent)));
    cout << "========================================" << endl;
  }
}

void createTrackbars() {
  namedWindow(TRACKBARS, WINDOW_NORMAL);
  createTrackbar("processNoiseCov_mantissa"     , TRACKBARS, &processNoiseCov_mantissa    , 200, updateFilter);
  createTrackbar("processNoiseCov_exponent"     , TRACKBARS, &processNoiseCov_exponent    , 20 , updateFilter);
  createTrackbar("measurementNoiseCov_mantissa" , TRACKBARS, &measurementNoiseCov_mantissa, 200, updateFilter);
  createTrackbar("measurementNoiseCov_exponent" , TRACKBARS, &measurementNoiseCov_exponent, 20 , updateFilter);
  createTrackbar("errorCovPost_mantissa"        , TRACKBARS, &errorCovPost_mantissa       , 200, updateFilter);
  createTrackbar("errorCovPost_exponent"        , TRACKBARS, &errorCovPost_exponent       , 20 , updateFilter);
}

  
int main() {
  namedWindow(WINDOW);
  createTrackbars();
  setMouseCallback(WINDOW, updateMousePos);
   
  // intialization of KF...
  KF.transitionMatrix = (Mat_<float>(4, 4) << 1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1);
  Mat_<float> measurement(2,1); measurement.setTo(Scalar(0));
   
  KF.statePre.at<float>(0) = mousePos.x;
  KF.statePre.at<float>(1) = mousePos.y;
  KF.statePre.at<float>(2) = 0;
  KF.statePre.at<float>(3) = 0;

  setIdentity(KF.measurementMatrix);
  //setIdentity(KF.processNoiseCov, Scalar::all(1e-4));
  //setIdentity(KF.measurementNoiseCov, Scalar::all(10));
  //setIdentity(KF.errorCovPost, Scalar::all(.1));
 
  updateFilter();

  // Image to show mouse tracking
  mousev.clear();
  kalmanv.clear();

  while (true) {
    // First predict, to update the internal statePre variable
    Mat prediction = KF.predict();
    Point2i predictPt(prediction.at<float>(0),prediction.at<float>(1));
                 
    // Get mouse point
    measurement(0) = mousePos.x;
    measurement(1) = mousePos.y;
     
    // The update phase
    Mat estimated = KF.correct(measurement);
    
    Point2i statePt(estimated.at<float>(0),estimated.at<float>(1));
    Point2i measPt(measurement(0),measurement(1));
    // plot points
    imshow(WINDOW, img);
    img = Scalar::all(0);
   
    mousev.push_back(measPt);
    kalmanv.push_back(statePt);

    static const float SCALE = 16;
    Point2i velocity(round(SCALE * estimated.at<float>(2)), round(SCALE * estimated.at<float>(3)));
    arrowedLine(img, statePt, statePt + velocity, Scalar(0, 0, 255), 2, LINE_AA);

    drawCross( statePt, Scalar(255,255,255), 5 );
    drawCross( measPt, Scalar(0,0,255), 5 );
   
    for (int i = 0; i < static_cast<int>(mousev.size())-1; i++)
      line(img, mousev[i], mousev[i+1], Scalar(255,255,0), 1);
     
    for (int i = 0; i < static_cast<int>(kalmanv.size())-1; i++)
      line(img, kalmanv[i], kalmanv[i+1], Scalar(0,155,255), 1);
     
    switch (static_cast<char>(waitKey(10))) {
    case 'q':
      return 0;
    case ' ':
      updateFilter();
      break;
    default:
      break;
    }
  }

  return 0;
}
