// from Andrew Keller
#include "Timestamp.h"

#include <fstream>
#include <iomanip>

std::ostream& operator<<(std::ostream& os, const Time& time) {
  return os << time.sec << '.' << std::setw(3) << std::setfill('0') << time.msec;
}

// This is the ugly parsing image function
Time getNextImage(std::ifstream& myFile, std::vector<char>& imageArray) {
  imageArray.clear();
  char buffer[4];
  Time timestamp;
  bool foundImage = false;

  while (!foundImage){
    myFile.read(buffer,1);

    if ((*buffer) == (char)0xFF) {
      myFile.read(buffer,1);

      if ((*buffer) == (char)0xD8) {
        //printf("found start of image \n");
        imageArray.push_back((char)0xFF);
        imageArray.push_back((char)0xD8);

        while (true) {
          myFile.read(buffer,1);
          imageArray.push_back(*buffer);

          if ((*buffer) == (char)0xFF) {
            myFile.read(buffer,1);
            imageArray.push_back(*buffer);

            if ((*buffer) == (char)0xFE) {
              myFile.read(buffer,4);
              imageArray.push_back(*buffer);
              imageArray.push_back(*(buffer+1));
              imageArray.push_back(*(buffer+2));
              imageArray.push_back(*(buffer+3));

              if ((*(buffer+3)) == (char)0x01) {
                myFile.read(buffer,4);
                unsigned int sec = 0;
                for (int i = 0; i < 4; i++){
                  imageArray.push_back(*(buffer + i));
                  sec <<= 8;
                  sec += *(unsigned char*)(void*)(buffer+i);
                }

                myFile.read(buffer,1);
                unsigned int hundreds = 0;
                imageArray.push_back(*buffer);
                hundreds += *(unsigned char*)(void*)buffer;
                timestamp.sec = sec;
                timestamp.msec = hundreds * 10;
              }
            }
            else if ((*buffer) == (char)0xD9){
              //printf("found end of image\n");
              foundImage = true;
              return timestamp;
            }
          }
        }
      }
    }
  }

  return Time{0,0};
}
