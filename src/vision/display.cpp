#include <opencv2/opencv.hpp>

using namespace cv;

static const char* RASPBERRY_PI_URL = "http://192.168.1.126:8080/?action=stream?dummy=param.mjpg";
static const char* ANDROID_URL = "http://192.168.1.125:8080/video?dummy=param.mjpg";
static const char* IP_CAM_URL = "http://192.168.1.90/mjpg/video.mjpg";
/**
 * @file vision.cpp
 */
int main(int argc, char* argv[]) {
  using namespace cv;
  using std::endl;
  using std::cerr;
  using std::cout;

  const char* uri = nullptr;

  bool image = false;

  // parse command line options
  for (int i = 1; i < argc; ++i) {
    if (argv[i][0] == '-') {
      for (char *c = &argv[i][1]; *c != '\0'; ++c) {
        switch (*c) {
        case 'i': image = true; break;
        default:
          printf("Unknown option \"%s\".\n", argv[i]);
          return 1;
        }
      }
    }
    else {
      uri = argv[i];
    }
  }

  const char* WINDOW = "Image";

  namedWindow(WINDOW);

  if (image) {
    if (uri == nullptr) uri = "sample_image.jpg";
    
    cout << "Reading image " << uri << endl;

    while (true) {
    cout << "Reading image " << uri << endl;
      Mat frame = imread(uri);

      imshow(WINDOW, frame);

      cv::waitKey(1);
    }
  }
  else {
    if (uri == nullptr) uri = IP_CAM_URL;

    cout << "Attempting to open " << uri << endl;

    VideoCapture video(uri);

    if (!video.isOpened()) {
      cout << "Failed to open stream to \"" << uri << "\"." << endl;
      //return 1;
    }
    else {
      cout << "Successfully opened \"" << uri << "\"." << endl;
    }

    while (true) {
      Mat frame;
      video >> frame;

      imshow(WINDOW, frame);
      cv::waitKey(10);

      //loop_rate.sleep();
    }
  }

  return 0;
}
