#ifndef OBJECT_H
#define OBJECT_H

#include <robot_soccer/Object.h>

#include "Vector2.h"

/**
 * An object has a position, orientation, and velocity.
 */
struct Object {
  /** The objects that are tracked in a game */
  enum Type {
      Robot1    = 0,
      //Robot2    = 1,
      Opponent1 = 1,
      //Opponent2 = 3,
      Ball      = 2,
      COUNT     = 3,
  };

  Point pos;
  float orientation;

  Vector vel;

  /** Initialize to "unknown" */
  Object();

  Object(const Point& position, const Vector& velocity, float orientation);

  /** Initialize to known position, orientation, and velocity */
  Object(Point position, float angle = unknown<float>(), float vx = unknown<float>(), float vy = unknown<float>());

  /** Initialize to known position, orientation, and velocity */
  Object(float x, float y, float angle = unknown<float>(), float vx = unknown<float>(), float vy = unknown<float>());

  void setVelocity(const Object& prev, float time_s);

  void set(const Point& position, const Vector& velocity, float angle) {
    pos = position;
    orientation = angle;
    vel = velocity;
  }

  void set(const Vector2<double>& position, const Vector2<double>& velocity, double angle) {
    pos.x = static_cast<float>(position.x);
    pos.y = static_cast<float>(position.y);
    vel.x = static_cast<float>(velocity.x);
    vel.y = static_cast<float>(velocity.y);
    orientation = static_cast<float>(angle);
  }

  /**
   * Construct from a ROS message.
   */
  Object(const robot_soccer::Object& object);

  /**
   * Assignment from a ROS message.
   */
  Object& operator=(const robot_soccer::Object& object);

  /**
   * Implicit cast to ROS message object.
   * This is probably a bad idea... a regular function would probably be better
   */
  robot_soccer::Object toROS() const;
};

/**
 * Print the contents of this object.
 */
std::ostream& operator<<(std::ostream& os, const Object& obj);

#endif
