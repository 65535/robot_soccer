#include "ColorFilter.h"

#include <fstream>

ColorFilter::ColorFilter(const std::string& filename) : source_file_(filename), h(-HUE_OFFSET, HUE_OFFSET-1), s(0, 255), v(0, 255) {
  std::ifstream file(source_file_);

  if (file.is_open()) {
    file >> h.min >> h.max;
    file >> s.min >> s.max;
    file >> v.min >> v.max;
    file >> morph_size;
    file >> min_size >> max_size;
  }
  else {
    std::cerr << "WARNING: Failed to open ColorFilter file " << filename << '.' << std::endl;
    morph_size = DEFAULT_MORPH_SIZE;
    min_size = DEFAULT_MIN_SIZE;
    max_size = DEFAULT_MAX_SIZE;
  }
}

ColorFilter::ColorFilter()
    : morph_size(DEFAULT_MORPH_SIZE),
      min_size(DEFAULT_MIN_SIZE), max_size(DEFAULT_MAX_SIZE),
      h(0, HUE_OFFSET-1), s(0, 255), v(0, 255) {}

Mat ColorFilter::postprocess(const Mat& threshold) const {
  using namespace cv;

  if (morph_size > 0) {
    Mat element = getStructuringElement(
        //MORPH_RECT,
        MORPH_ELLIPSE,
        morphSize(),
        Point2i(morph_size, morph_size)
    );

    Mat eroded, dilated;
    erode(threshold, eroded, element);
    dilate(eroded, dilated, element);

    return dilated;
  }
  else {
    return threshold;
  }
}

Mat ColorFilter::filter(const Mat& hsv) const {
  Mat filtered;

  // three cases:
  // 1. normal (not split around 0)
  // 2. negative min, positive max
  // 3. negative min, negative max
  if (h.min >= 0) {
    inRange(hsv, min(), max(), filtered);
    return postprocess(filtered);
  }
  else {
    if (h.max >= 0) {
      Mat other_filtered;
      inRange(hsv, Scalar(0, s.min, v.min), max(), filtered);
      inRange(hsv, negative_min(), Scalar(h.maxValue(), s.max, v.max), other_filtered);

      return postprocess(filtered | other_filtered);
    }
    else {
      inRange(hsv, negative_min(), negative_max(), filtered);
      return postprocess(filtered);
    }
  }
}

const char* ColorFilter::sourceFilename() const {
  size_t last_slash = source_file_.find_last_of('/');

  if (last_slash == std::string::npos) {
    return source_file_.c_str();
  }
  else {
    return &source_file_[last_slash+1];
  }
}

void ColorFilter::save(const std::string& filename) {
  if (!filename.empty()) source_file_ = filename;

  // Beware: this quietly fails if the filename is invalid
  std::ofstream file(source_file_);
  file << *this;
}

void ColorFilter::reset() {
  std::string source(source_file_);
  *this = ColorFilter();
  this->source_file_ = std::move(source);
}

std::ostream& operator<<(std::ostream& os, const ColorFilter& flt) {
  os << flt.h.min << ' ' << flt.h.max << '\n';
  os << flt.s.min << ' ' << flt.s.max << '\n';
  os << flt.v.min << ' ' << flt.v.max << '\n';
  os << flt.morph_size << '\n';
  os << flt.min_size << ' ' << flt.max_size << std::endl;

  return os;
}
