#ifndef COLORFILTER_H
#define COLORFILTER_H

#include <opencv2/opencv.hpp>
#include <string>

using cv::Mat;
using cv::Scalar;

/**
 * A range of HSV values that describe a color to look for.
 */
class ColorFilter {
private:
  static const int DEFAULT_MORPH_SIZE = 2;
  static const int DEFAULT_MIN_SIZE = 1;
  static const int DEFAULT_MAX_SIZE = 10000;

  std::string source_file_;

  /**
   * Process a threshold image to remove noise and emphasize white areas.
   * 
   * @param threshold a binary image for processing
   * @return the processed version threshold image
   */
  Mat postprocess(const Mat& threshold) const;
  
public:
  /**
   * How much the hue is offset if using 0-359 for hue
   * OpenCV uses 0-179 for hue, but this splits red across 0, making it hard to filter.
   * Offsetting the hue gives double coverage of the spectrum, so no colors are split in half.
   */
  static const int HUE_OFFSET = 180;

  /** One channel of the color. */
  class Channel {
    int minValue_;
    int maxValue_;

  public:
    int min;
    int max;

    /** Get the minimum allowed value for this channel. */
    inline int minValue() const { return minValue_; }

    /** Get the maximum allowed value for this channel. */
    inline int maxValue() const { return maxValue_; }

    /** Construct a new color channel */
    inline Channel(int minVal, int maxVal) : minValue_(minVal), maxValue_(maxVal), min(minVal), max(maxVal) {}
  };

  int morph_size;
  double min_size, max_size;
  Channel h;
  Channel s;
  Channel v;

  /**
   * Read in a ColorFilter from disk.
   *
   * No error checking whatsoever is done. So be careful.
   */
  ColorFilter(const std::string& filename);
  
  /**
   * Create a filter that accepts any HSV values.
   */
  ColorFilter();

  /**
   * Apply this filter to an image.
   */
  Mat filter(const Mat& hsv) const;

  /** Get the Scalar object with the minimums for filtering with inRange
   *  If h.min is negative, 0 is returned.
   */
  inline Scalar min() const { return Scalar(h.min, s.min, v.min); }

  /** Get the Scalar object with the maximums for filtering with inRange */
  inline Scalar max() const { return Scalar(h.max, s.max, v.max); }

  /** Get the Scalar object with the minimums for filtering with inRange
   *  Assumes h.min is negative and offsets it accordingly.
   */
  inline Scalar negative_min() const { return Scalar(HUE_OFFSET + h.min, s.min, v.min); }

  /** Get the Scalar object with the maximums for when max hue is negative
   *  Assumes h.max is negative and offsets it accordingly.
   */
  inline Scalar negative_max() const { return Scalar(HUE_OFFSET + h.max, s.max, v.max); }

  /** Set the min and max hue if provided in the range 0-359 */
  inline void setShiftedHue(int h_min, int h_max) {
    h.min = h_min - HUE_OFFSET;
    h.max = h_max - HUE_OFFSET;
  }

  /** Get the min hue in the range 0-359 */
  int shiftedMinHue() { return h.min + HUE_OFFSET; }

  /** Get the max hue in the range 0-359 */
  int shiftedMaxHue() { return h.max + HUE_OFFSET; }

  /** Get the morph size as a cv::Size */
  inline cv::Size morphSize() const {
    return cv::Size(2*morph_size + 1, 2*morph_size + 1);
  }
  
  /**
   * Get the source filename of this ColorFilter.
   * @return a C-string of the filename without the directory
   */
  const char* sourceFilename() const;

  /**
   * Save this ColorFilter to disk.
   * @param filename the name of the file on disk; if blank, the original source file is used
   */
  void save(const std::string& filename = {});

  /**
   * Reset the filter.
   */
  void reset();
};

/** Print the ranges of this filter. */
std::ostream& operator<<(std::ostream& os, const ColorFilter& flt);

#endif
