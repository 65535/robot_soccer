#include "Filter.h"

void Filter::setTimeDelta(T dt) {
  //  << 1,0,t,0,0,0 x
  //     0,1,0,t,0,0 y
  //     0,0,1,0,0,0 Vx
  //     0,0,0,1,0,0 Vy
  //     0,0,0,0,1,t angle
  //     0,0,0,0,0,1 angular velocity

  if (track_ >= ANGULAR_VEL) {
    filter_.transitionMatrix.at<T>(4, 5) = dt;
  }

  if (track_ >= VELOCITY) {
    filter_.transitionMatrix.at<T>(0, 2) = dt;
    filter_.transitionMatrix.at<T>(1, 3) = dt;
  }
}

Filter::Filter(Level to_track, Level to_measure, T process_noise_cov, T measurement_noise_cov, T error_cov) 
    : track_(to_track), measure_(to_measure),
      filter_(track_, measure_, 0, MAT_TYPE), // TODO: add control variables?
      measurement_(measure_, 1, MAT_TYPE) {
  CV_DbgAssert(track_ >= measure_);

  filter_.transitionMatrix = cv::Mat::eye(track_, track_, MAT_TYPE);

  setIdentity(filter_.measurementMatrix);

  // TODO: configure these individually
  setIdentity(filter_.processNoiseCov, cv::Scalar::all(process_noise_cov));
  setIdentity(filter_.measurementNoiseCov, cv::Scalar::all(measurement_noise_cov));
  setIdentity(filter_.errorCovPost, cv::Scalar::all(error_cov));
}

void Filter::setAllCovariances(T process, T measurement, T error) {
  setIdentity(filter_.processNoiseCov, cv::Scalar::all(process));
  setIdentity(filter_.measurementNoiseCov, cv::Scalar::all(measurement));
  setIdentity(filter_.errorCovPost, cv::Scalar::all(error));
}

void Filter::setCovariance(Variable var, T process, T measurement, T error) {
  filter_.processNoiseCov.at<T>(var, var) = process;
  filter_.measurementNoiseCov.at<T>(var, var) = process;
  filter_.errorCovPost.at<T>(var, var) = error;
}

void Filter::setInitialConditions(const Vector2<T>& init_pos, const Vector2<T>& init_vel, T init_orient, T init_angular_velocity) {
  // always track position
  filter_.statePre.at<T>(0) = init_pos.x;
  filter_.statePre.at<T>(1) = init_pos.y;

  if (track_ >= VELOCITY) {
    filter_.statePre.at<T>(2) = init_vel.x;
    filter_.statePre.at<T>(3) = init_vel.y;
  }

  if (track_ >= ORIENTATION) {
    filter_.statePre.at<T>(4) = init_orient;
  }

  if (track_ >= ANGULAR_VEL) {
    filter_.statePre.at<T>(5) = init_angular_velocity;
  }
}

void Filter::correct(const Vector2<T>& pos) {
  measurement_.at<T>(0) = pos.x;
  measurement_.at<T>(1) = pos.y;

  state_ = filter_.correct(measurement_);
}

void Filter::correct(const Vector2<T>& pos, const Vector2<T>& vel) {
  measurement_.at<T>(0) = pos.x;
  measurement_.at<T>(1) = pos.y;
  measurement_.at<T>(2) = vel.x;
  measurement_.at<T>(3) = vel.y;
  
  state_ = filter_.correct(measurement_);
}

void Filter::correct(const Vector2<T>& pos, const Vector2<T>& vel, T orientation) {
  measurement_.at<T>(0) = pos.x;
  measurement_.at<T>(1) = pos.y;
  measurement_.at<T>(2) = vel.x;
  measurement_.at<T>(3) = vel.y;
  measurement_.at<T>(4) = orientation;

  state_ = filter_.correct(measurement_);
}

void Filter::correct(const Vector2<T>& pos, const Vector2<T>& vel, T orientation, T angular_vel) {
  measurement_.at<T>(0) = pos.x;
  measurement_.at<T>(1) = pos.y;
  measurement_.at<T>(2) = vel.x;
  measurement_.at<T>(3) = vel.y;
  measurement_.at<T>(4) = orientation;
  measurement_.at<T>(5) = angular_vel;
  
  state_ = filter_.correct(measurement_);
}
