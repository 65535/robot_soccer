#ifndef TIMESTAMP_H
#define TIMESTAMP_H

#include <iostream>
#include <vector>

struct Time {
  unsigned int sec;
  unsigned int msec;

  double seconds() const {
    return sec + static_cast<double>(msec) / 1000.0;
  }

  unsigned long milliseconds() const {
    return static_cast<unsigned long>(sec) * 1000 + msec;
  }
};

std::ostream& operator<<(std::ostream& os, const Time& time);

Time getNextImage(std::ifstream & myFile, std::vector<char> & imageArray);

#endif
