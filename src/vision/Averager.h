#ifndef AVERAGER_H
#define AVERAGER_H

/** Average a bunch of values */
template<typename T = double>
class Averager {
  unsigned long count_;
  T total_;

public:
  constexpr Averager() : count_(0), total_() {}

  void add(T value) {
    total_ += value;
    count_  += 1;
  }

  T average() const { return total_ / static_cast<T>(count_); }

  T total() const { return total_; }

  unsigned long count() const { return count_; }
};

/** Maintain a running average of the last VALUES values */
template<unsigned VALUES, typename T = double>
class RunningAverager {
  T values_[VALUES];
  T running_total_;
  unsigned next_;

  inline void increment(unsigned* variable) {
    *variable = (*variable == VALUES-1) ? 0 : *variable + 1;
  }

public:
  constexpr RunningAverager() : values_{}, running_total_(), next_(0) {}

  void add(const T& value) {
    running_total_ -= values_[next_];
    running_total_ += value;
    values_[next_] = value;

    increment(&next_);
  }

  T average() const { return running_total_ / static_cast<T>(VALUES); }

  T total() const { return running_total_; }

  constexpr unsigned count() const { return VALUES; }

  // iterator
  typedef const T* const_iterator;
  const_iterator begin() const { return values_; }
  const_iterator end() const { return &values_[VALUES]; }
};

#endif
