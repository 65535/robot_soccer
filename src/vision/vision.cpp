#include "GameState.h"
#include "Tracker.h"
#include "Timestamp.h"

#include "Averager.h"
#include "CycleTimer.h"

#include <ros/ros.h>
#include <robot_soccer/GameState.h>

#include <fstream>

namespace {
  const char* IP_CAM_URL = "http://192.168.1.90/mjpg/video.mjpg";
  //const char* RASPBERRY_PI_URL = "http://192.168.1.126:8080/?action=stream?dummy=param.mjpg";
  //const char* ANDROID_URL = "http://192.168.1.125:8080/video?dummy=param.mjpg";

  const char* DEFAULT_URL = IP_CAM_URL;

  enum VideoSource {
    Image,
    IPWebcamApp,
    URL,
    URLWithTimestamp,
    USB,
  };
}

void printUsage(const char* prog_name) {
  std::cerr << "Usage:\n"
            << "\trosrun robot_soccer vision_node [-aghiqw] [URI]\n"
               "\n"
               "\tURI\tThe location of the file or video stream, or the IP address if -w is used\n"
               "\t\tDefaults to sample_image.jpg for images and " << DEFAULT_URL << " for video.\n"
               "\n"
               "\t-a\tTrack the away team. Defaults to tracking the home team\n"
               "\t-h\tDisplay this message. In case you forgot something?\n"
               "\t-i\tUse a static image instead of a video feed\n"
               "\t-n\tDo not get the timestamp from the URL\n"
               "\t-q\tDo not use the calibration GUI\n"
               "\t-t\tUse multiple threads for object detection\n"
               "\t-u\tUse a USB camera as the image source\n"
               "\t-w\tFind video being streamed by the IP Webcam app\n"
               "\t\tThe URI argument is the IP address of the phone.\n"
            << std::endl;
}

bool openVideo(cv::VideoCapture* video, VideoSource source, const std::string& arg) {
  using std::cout;
  using std::cerr;
  using std::endl;

  std::string uri(DEFAULT_URL);

  switch (source) {
  case IPWebcamApp:
    uri = "http://" + arg + ":8080/video?type=.mjpg";
    video->open(uri);
    break;
  case URL:
    if (!arg.empty()) uri = arg;
    video->open(uri);
    break;
  case USB:
    for (int i = 0; !video->isOpened() && i < 6; ++i) {
      video->open(i);
    }

    uri = "USB";
    break;
  case URLWithTimestamp:
    // TODO
    break;
  default:
    cerr << "Inavlid VideoSource" << endl;
    exit(1);
  }
    

  if (video->isOpened() || video->open(uri)) {
    cout << "Successfully opened \"" << uri << "\"." << endl;
    return true;
  }
  else {
    cout << "Failed to open stream to \"" << uri << "\"." << endl;
    return false;
  }
}

inline double currentTime() {
  return std::chrono::high_resolution_clock::now().time_since_epoch().count() / 1e9;
}

void urlWithTimestamp(const ros::Publisher& state_publisher, FieldInfo::Team team, bool multithread, bool gui) {
  using namespace cv;

  // TODO: this is poor form, just saying
  int CV_LOAD_IMAGE_COLOR = 1;

  system("curl -s http://192.168.1.90/mjpg/video.mjpg > imagefifo &");

  std::ifstream fifo("imagefifo", std::ifstream::binary);
  std::vector<char> image_array;

  Time timestamp = getNextImage(fifo, image_array);
  Mat frame = imdecode(image_array, CV_LOAD_IMAGE_COLOR);

  Tracker tracker(team, frame, multithread, gui);

  image_array.clear();

  using namespace std;
  RunningAverager<32> avg, time;
  int i = 1;
  Timer timer;

  while (true) {
    timestamp = getNextImage(fifo, image_array);
    timer.start();
      avg.add(currentTime() - timestamp.seconds());
    frame = imdecode(image_array, CV_LOAD_IMAGE_COLOR);

    robot_soccer::GameState state(tracker.update(frame).toROS());
    state.timestamp = timestamp.milliseconds();
    state_publisher.publish<robot_soccer::GameState>(state);
    timer.stop();
    time.add(timer.time<Timer::ms>());

      if (i++ % 32 == 0) {
        std::cout << avg.average() << std::endl;
        std::cout << time.average() << std::endl;
      }
    //printf("%d.%03d\n", timestamp.sec, timestamp.msec);
  }
}

/**
 * @file vision.cpp
 */
int main(int argc, char* argv[]) {
  using namespace cv;
  using std::endl;
  using std::cerr;
  using std::cout;

  // settings
  std::string uri;
  bool gui = true;
  bool multithread = false;
  VideoSource source = URLWithTimestamp;
  FieldInfo::Team team = FieldInfo::Home;

  // parse command line options
  for (int i = 1; i < argc; ++i) {
    if (argv[i][0] == '-') {
      for (char *c = &argv[i][1]; *c != '\0'; ++c) {
        switch (*c) {
        case 'a': team = FieldInfo::Away; break;
        case 'i': source = Image; break;
        case 'h': printUsage(argv[0]); return 0;
        case 'n': source = URL; break;
        case 'q': gui = false; break;
        case 't': multithread = true; cerr << "Multithreading is still trying to track four robots, so it does not work!"; break;
        case 'u': source = USB; break;
        case 'w': source = IPWebcamApp; break;
        default:
          printf("Unknown option \"%s\".\n", argv[i]);
          printUsage(argv[0]);
          return 1;
        }
      }
    }
    else {
      uri = argv[i];
    }
  }

  // Initialize vision node with ROS
  ros::init(argc, argv, "vision");
  ros::NodeHandle handle;
  ros::Publisher state_publisher = handle.advertise<robot_soccer::GameState>("state", 5);

  // TODO: use the actual loop rate for updates
  ros::Rate loop_rate(1/2.0);

  // Nothing is published if nothing is found -- could publish unknown if desired:
  //  state_publisher.publish<robot_soccer::GameState>(GameState<2>::unknown());
  if (source == Image) {
    if (uri.empty()) uri = "sample_image.jpg";
    
    cout << "Reading image " << uri << endl;

    Tracker tracker(team, imread(uri), multithread, gui);

    while (ros::ok()) {
      Mat frame = imread(uri);

      state_publisher.publish<robot_soccer::GameState>(tracker.update(frame).toROS());

      //loop_rate.sleep();
    }
  }
  else {
    if (source == URLWithTimestamp) {
      urlWithTimestamp(state_publisher, team, multithread, gui);
    }
    else {
      VideoCapture video;
      if (!openVideo(&video, source, uri)) return 1;

      Mat frame;
      video >> frame;

      Tracker tracker(team, frame, multithread, gui);

      while (ros::ok()) {
        uint32_t timestamp = static_cast<uint32_t>(Timer::runningTime<Timer::ms>());
        video >> frame;

        robot_soccer::GameState state(tracker.update(frame).toROS());
        state.timestamp = timestamp;
        
        state_publisher.publish<robot_soccer::GameState>(state);
      }
    }
  }

  return 0;
}
