#include "ImageProcessor.h"

#include <fstream>
#include <cassert>

ImageProcessor::ImageProcessor(FieldInfo::Team side, const Mat& sample, int color_conversion)
    : undistorted_size_(sample.cols + 2*X_SHIFT, sample.rows + 2*Y_SHIFT),
      field_(FIELD_CONFIG, side, undistorted_size_.width, undistorted_size_.height, 8), // TODO: added 8 pixel border
      color_conversion_(color_conversion) {
  readFilterConfig();
  loadCameraProfile(sample);
}

void ImageProcessor::readFilterConfig() {
  std::ifstream config_file(FILTER_CONFIG);

  // Read in the filters from disk
  std::string filter_name;
  
  // read in filter configs
  for (int i = 0; i < Object::COUNT; ++i) {
    config_file >> filter_name;
    filters_[i] = ColorFilter(FILTERS_DIR + filter_name);
  }

  // read in blur size
  int blur;
  config_file >> blur;
  blur_size_ = cv::Size(blur, blur);
}

void ImageProcessor::saveFilterConfig() const {
  std::ofstream file(FILTER_CONFIG);

  for (int i = 0; i < Object::COUNT; ++i) {
    file << filters_[i].sourceFilename() << '\n';
  }

  file << blur_size_.width << std::endl;
}

void ImageProcessor::loadCameraProfile(const Mat& sample) {
  using namespace cv;

  Mat camera_matrix, distortion_coeff;

  FileStorage fs(CAMERA_INFO, FileStorage::READ);
  fs["camera_matrix"] >> camera_matrix;
  fs["distortion_coefficients"] >> distortion_coeff;
  fs.release();

  Mat new_camera_matrix = camera_matrix.clone();
  new_camera_matrix.at<double>(0, 2) += X_SHIFT;
  new_camera_matrix.at<double>(1, 2) += Y_SHIFT;

  initUndistortRectifyMap(
      camera_matrix, distortion_coeff, Mat(), 
      new_camera_matrix,
      undistorted_size_,
      CV_16SC2, map1_, map2_
  );
  
  createInverseMap(sample);
}

void ImageProcessor::createInverseMap(const Mat& sample) {
  map1_inv_ = Mat(sample.size(), map1_.type());
  map1_inv_.setTo(cv::Scalar(unknown<short>(), unknown<short>()));

  for (short r = 0; r < map1_.size().height; ++r)
  for (short c = 0; c < map1_.size().width; ++c) {
    cv::Vec2s source(map1_.at<cv::Vec2s>(r, c));
    //std::cout << source << " is from " << r << ',' << c << std::endl;
    if (source[0] >= 0 && source[0] < map1_inv_.cols
        && source[1] >= 0 && source[1] < map1_inv_.rows) {
      map1_inv_.at<cv::Vec2s>(source[1], source[0]) = cv::Vec2s{r, c};
    }
  }
}

Mat ImageProcessor::undistort(const Mat& frame) const {
  Mat remapped;
  remap(frame, remapped, map1_, map2_, cv::INTER_LINEAR);
  return remapped;
}

Mat ImageProcessor::crop(const Mat& frame) const {
  return Mat(frame, field_.boundingRectangle<cv::Rect>());
}

Mat ImageProcessor::preprocess(const Mat& frame) const {
  Mat hsv;
  if (blur_size_.width > 0) {
    Mat blur;
    GaussianBlur(frame, blur, blur_size_, 0, 0);
    cvtColor(blur, hsv, color_conversion_);
  }
  else {
    cvtColor(frame, hsv, color_conversion_);
  }
  
  return hsv;
}

Mat ImageProcessor::filterObject(const Mat& hsv, Object::Type object) const {
  assert(object >= 0 && object < Object::COUNT);
  return filters_[object].filter(hsv);
}

Mat ImageProcessor::filterRobot(const Mat& hsv, Object::Type robot) const {
  assert(robot >= Object::Robot1 && robot < Object::COUNT-1); // exclude the ball
  return filters_[robot].filter(hsv);
}

Mat ImageProcessor::filterBall(const Mat& hsv) const {
  return filters_[Object::Ball].filter(hsv);
}

Mat ImageProcessor::manualRemap(Mat frame) const {
  Mat img(undistorted_size_, frame.type());
  //Mat img = Mat::zeros(undistorted_size_, frame.type());

  for (int x = 0; x < img.size().width; ++x)
  for (int y = 0; y < img.size().height; ++y) {
    cv::Vec2s pixel(map1_.at<cv::Vec2s>(y, x));

    if (pixel[1] >= 0 && pixel[1] < frame.rows && pixel[0] >= 0 && pixel[0] < frame.cols) {
      img.at<cv::Vec3b>(y, x) = frame.at<cv::Vec3b>(pixel[1], pixel[0]);
    }
  }

  return img;
}

Pixel ImageProcessor::undistortPixel(Pixel pixel) const {
  cv::Vec2s new_pixel = map1_inv_.at<cv::Vec2s>(pixel.y, pixel.x);

  return Pixel(
      new_pixel[1],
      new_pixel[0]
  );
}

void ImageProcessor::saveFilter(Object::Type object, const std::string& file) {
  if (file.empty()) {
    filters_[object].save();
  }
  else {
    filters_[object].save(FILTERS_DIR + file);
    saveFilterConfig();
  }
}

bool ImageProcessor::saveField(const Pixel corners[4]) {
  return field_.save(corners);
}
