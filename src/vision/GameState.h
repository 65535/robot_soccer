#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <robot_soccer/GameState.h>

#include <iostream>
#include <cassert>

#include "Object.h"

/**
 * The state of the game.
 *
 * TODO: This object could just be a struct w/ no functions
 */
// TODO: Doesn't need to be a template. A constant could be better.
class GameState {
  Object objects_[Object::COUNT];

public:
  GameState() {}

  GameState(const Object objs[Object::COUNT]) {
    for (int i = 0; i < Object::COUNT; ++i) {
      objects_[i] = objs[i];
    }
  }

  void setVelocities(const GameState& prev, float delta_s) {
    for (int i = 0; i < Object::COUNT; ++i) {
      objects_[i].setVelocity(prev.objects_[i], delta_s);
    }
  }

  void predictPositionFromPrevious(const GameState& prev, float delta_s) {
    for (int i = 0; i < Object::COUNT; ++i) {
      if (isUnknown<float>(objects_[i].pos.x)) {
        //if (isUnknown<float>(objects_[i].vel.x)) {
          objects_[i].pos = prev.objects_[i].pos;
        //}
        //else {
        //  objects_[i].pos = prev.objects_[i].pos + (prev.objects_[i].vel * delta_s);
        //}
      }
    }
  }

  /**
   * Construct a GameState object from a ROS GameState message
   */
  GameState(const robot_soccer::GameState& ros_msg) {
    for (int i = 0; i < Object::COUNT; ++i) {
      objects_[i] = ros_msg.objects[i];
    }
  }

  /**
   * Return a GameState with unknown values.
   */
  static GameState unknown() {
    return GameState();
  }

  // Objects //
  const Object& object(int i) const {
    assert(i >= 0 && i < Object::COUNT);
    return objects_[i];
  }

  Object& operator[](int i) {
    assert(i >= 0 && i < Object::COUNT);
    return objects_[i];
  }

  const Object& operator[](int i) const {
    assert(i >= 0 && i < Object::COUNT);
    return objects_[i];
  }

  /**
   * Convert GameState to GameState ROS message.
   */
  robot_soccer::GameState toROS() const {
    robot_soccer::GameState state;

    for (int i = 0; i < Object::COUNT; ++i) {
      state.objects.push_back(objects_[i].toROS());
    }
    
    return state;
  }
};

#endif
