#ifndef FIELD_H
#define FIELD_H

#include "Vector2.h"
#include "FieldInfo.h"

/**
 * @brief Convert pixel values to real world coordinates.
 *
 * The field is initialized based on calibration data in calibration/field.txt
 * This object is fed pixel values and spits out the field coordinates.
 */
class Field {
private:
  const char* config_file_;

  // midpoints of the sides of the fields in pixels relative to the defined corners
  //float p_ml_; //mid-left
  //float p_mr_; //mid-right
  //float p_mt_; //mid-top
  //float p_mb_; //mid-bottom

  // the bounds of the field
  Pixel topLeft_;
  Pixel botRight_;

  // The corners:
  // 0-----1
  // |     |
  // 2-----3
  Pixel corners_[4];
  Point center_;
  Vector scale_; // the scale for x and y separately in meters/pixel

  /** Whether playing as HOME or AWAY */
  FieldInfo::Team side_;

  // padding around the bounding rectangle
  int border_;

  int max_width_, max_height_;

  template<typename T>
  inline static T min(T a, T b) {
    return (a <= b ? a : b);
  }

  template<typename T>
  inline static T max(T a, T b) {
    return (a > b ? a : b);
  }

  /** Make the field take the whole image */
  void resetCorners();

  /** Check that the specified corners are on the image. */
  bool validCorners(const Pixel corners[4]) const;

  /** Check that the specified corners are on the image. */
  bool validCorners() const;

  /** Calculate the bounding box. */
  void calculateBounds();

  /**
   * Store the new corners. Order does not matter -- this function sorts out their position.
   * @param new_corners the new corners to store
   * @return true if the corners were valid and updated; false if the corners were invalid and nothing was done
   */
  bool storeCorners(const Pixel new_corners[4]);

public:
  /**
   * Initialize the field based on the values in calibration/field.txt
   * 
   * @param config_file the configuration file to use
   * @param side Which side the team is playing on (Home or Away).
   *             If the team is away, all of the values are inverted so that the 
   *             home strategies will work the same.
   * @param border how much padding to use around the bounding rectangle
   */
  Field(const char* config_file, FieldInfo::Team side, int width, int height, int border = 0);

  /**
   * @brief Convert from a pixel on the cropped field to real coordinates.
   * 
   * This uses the four corners of the field to *roughly* account for the skew of the field.
   * This function does not account for barrel distortion caused by the lens.
   * Barrel distortion should be corrected for before finding the objects in the image.
   *
   * @param pixel the pixel point on the cropped field image to convert
   * @return the point in the game coordinate system
   */
  inline Point toPosition(Pixel pixel) const {
    return toPosition(pixel.x, pixel.y);
  }

  /**
   * Convert a real coordinate to a pixel.
   * This doesn't always map back to the correct pixel because of rounding.
   * @param x the x coordinate in real-world coordinates
   * @param y the y coordinate in real-world coordinates
   */
  Pixel toPixel(float x, float y) const;

  /**
   * Convert a real coordinate to a pixel.
   * This doesn't always map back to the correct pixel because of rounding.
   * @param x the x coordinate in real-world coordinates
   * @param y the y coordinate in real-world coordinates
   */
  inline Pixel toPixel(Point point) const {
    return toPixel(point.x, point.y);
  }

  /**
   * @brief Convert from a pixel on the cropped field to real coordinates.
   * 
   * This uses the four corners of the field to *roughly* account for the skew of the field.
   * This function does not account for barrel distortion caused by the lens.
   * Barrel distortion should be corrected for before finding the objects in the image.
   *
   * @param pixel the pixel point on the cropped field image to convert
   * @return the point in the game coordinate system
   */
  Point toPosition(int pixel_x, int pixel_y) const;

  /** Convert a pixel from the full image to a pixel in the cropped image. */
  inline Pixel toCroppedCoordinates(const Pixel& p) const { return p - topLeft_ + Pixel(border_); }

  /** Convert a pixel from the full image to a pixel in the cropped image. */
  inline Pixel toCroppedCoordinates(int x, int y) const { return Pixel(x, y) - topLeft_ + Pixel(border_); }

  /** Convert a pixel from the cropped image to a pixel in the full image. */
  inline Pixel toFullCoordinates(int x, int y) const { return Pixel(x, y) + topLeft_ - Pixel(border_); }

  /** Convert a pixel from the cropped image to a pixel in the full image. */
  inline Pixel toFullCoordinates(Pixel p) const { return p + topLeft_ - Pixel(border_); }

  /** Get which side the field is set for */
  FieldInfo::Team side() const { return side_; }

  /**
   * Return the bounding rectangle of the field.
   *
   * @param Rect the type to use for the rectangle
   * @param padding an optional padding to add around the rectangle
   * @return the bounding rectangle
   */
  template<class Rect>
  inline Rect boundingRectangle() const {
    return Rect(topLeft_.x - border_, topLeft_.y - border_, botRight_.x-topLeft_.x + 2*border_, botRight_.y-topLeft_.y + 2*border_);
  }

  /**
   * Set the border around the bounding rectangle.
   * @param new_border the new border, in pixels
   */
  void setBorder(int new_border);

  /**
   * Save the current corners to disk.
   */
  void save() const;

  /**
   * Update the corners and save to disk.
   * Aborts if the new corners are invalid (off the image).
   * @param corners an array of the four new corners
   * @return true if the new corners were valid; false if not
   */
  bool save(const Pixel new_corners[4]);
};

#endif
