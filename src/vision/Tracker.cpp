#include "Tracker.h"

#include <cmath>
#include <fstream>
#include <thread>

constexpr const Object::Type Tracker::TO_TRACK[];

void Tracker::locateBall() {
  Pixel ball_location;

  static const auto findTheBall = [&ball_location](double, int x, int y) { 
    // make sure the ball hasn't been found yet
    if (ball_location.x < 0) {
      ball_location = Pixel(x, y);
      return true;
    }
    else {
      return false;
    }
  };

  if (locateObjects(images_[Object::Ball], processor_.objectFilter(Object::Ball), findTheBall)) {
    filter(Object::Ball, processor_.toPosition(ball_location));
  }
  else {
    filter(Object::Ball);
  }
}

// initialize a separate Kalman Filter for each object being tracked
void Tracker::initializeFilter() {
  static const filter_t PROCESS_COV = 1e5f;
  static const filter_t MEASUREMENT_COV = 4e6f;
  static const filter_t ERROR_COV = 10.0f;

  static const filter_t POS_MEASUREMENT_COV = 1e6f;
  static const filter_t ANGLE_MEASUREMENT_COV = 50.0f;

  static const Vector2<filter_t> STARTING[Object::COUNT] = {
    Point(-FieldInfo::WIDTH/2, 0).to<Vector2<filter_t>>(),
    //FieldInfo::OUR_GOAL_CENTER.to<Vector2<filter_t>>(),
    Point(FieldInfo::WIDTH/2, 0).to<Vector2<filter_t>>(),
    //FieldInfo::GOAL_CENTER.to<Vector2<filter_t>>(),
    FieldInfo::CENTER.to<Vector2<filter_t>>(),
  };

  for (int i = 0; i < Object::COUNT-1; ++i) {
    filters.emplace_back(Filter::ORIENTATION, Filter::ORIENTATION, PROCESS_COV, MEASUREMENT_COV, ERROR_COV);
    //filters.emplace_back(Filter::VELOCITY, Filter::VELOCITY, PROCESS_COV, MEASUREMENT_COV, ERROR_COV);
    filters.back().setCovariance(Filter::PosX, PROCESS_COV, POS_MEASUREMENT_COV, ERROR_COV);
    filters.back().setCovariance(Filter::PosY, PROCESS_COV, POS_MEASUREMENT_COV, ERROR_COV);
    filters.back().setCovariance(Filter::Angle, PROCESS_COV, ANGLE_MEASUREMENT_COV, ERROR_COV);
    filters.back().setInitialConditions(STARTING[i]);
  }

  filters.emplace_back(Filter::VELOCITY, Filter::VELOCITY, PROCESS_COV, MEASUREMENT_COV, ERROR_COV);
  filters.back().setInitialConditions(STARTING[Object::Ball]);
  filters.back().setCovariance(Filter::PosX, PROCESS_COV, POS_MEASUREMENT_COV, ERROR_COV);
  filters.back().setCovariance(Filter::PosY, PROCESS_COV, POS_MEASUREMENT_COV, ERROR_COV);
}

Tracker::Tracker(FieldInfo::Team side, const Mat& sample, bool multithread, bool use_gui) : side_(side), processor_(side_, sample) {
  if (use_gui) cal = new Calibrator(&processor_);
  else cal = nullptr;

  initializeFilter();

  if (multithread) {
    if (use_gui) {
      std::cout << "WARNING: The GUI may not function properly when running with multiple threads." << std::endl;
    }
    // TODO: just turn off the GUI when multi-threading

    initializeThreads();
    updateFunction_ = &Tracker::updateMulti;
  }
  else {
    updateFunction_ = &Tracker::updateSingle;
  }
}

Tracker::~Tracker() {
  delete cal;
}

const GameState& Tracker::updateSingle(const Mat& frame) {
  prev_state_ = state_;

  // prepare the frame
  Mat undistorted = processor_.undistort(frame);
  Mat cropped = processor_.crop(undistorted);
  hsv_ = processor_.preprocess(cropped);

  // TODO: CLEANUP THIS!
  delta_t = timeSinceLastCall<filter_t>();

  for (Object::Type obj : TO_TRACK) {
    images_[obj] = processor_.filterRobot(hsv_, obj);
    locateRobot(obj);
  }

  images_[Object::Ball] = processor_.filterBall(hsv_);
  locateBall();
  state_[Object::Ball].orientation = unknown<filter_t>(); // this is ugly code

  // if using the calibration GUI, draw the objects
  if (cal) {
    cal->drawGameState(cropped, state_);

    for (Object::Type obj : TO_TRACK) {
      cal->displayProcessed(images_[obj], obj);
    }

    cal->displayProcessed(images_[Object::Ball], Object::Ball);

    cal->displayOriginal(undistorted);
    cal->displayCropped(cropped);
    cal->waitForInput();
  }

  return state_;
}

namespace {
  /** A class with the function operator for finding a robot in locateObjects */
  class RobotFinder {
    Pixel* big_;
    Pixel* small_;
    int count_;
    double area_;

  public:
    constexpr RobotFinder(Pixel* big, Pixel* small) : big_(big), small_(small),
        count_(0), area_(0) {}

    /** Process a newly found blob in locateObjects.
      * Keep track of which blob is largest.
      */
    bool operator()(double area, int x, int y) {
      count_ += 1;

      if (count_ == 1) {
        area_ = area;
        big_->x = x;
        big_->y = y;
        return true;
      }
      else if (count_ == 2) {
        if (area > area_) {
          *small_ = *big_;
          *big_ = Pixel(x, y);
        }
        else {
          *small_ = Pixel(x, y);
        }
        return true;
      }
      else {
        return false;
      }
    }
  };
}

void Tracker::filter(Object::Type object) {
  filters[object].setTimeDelta(delta_t);
  filters[object].predict();

  state_[object].set(filters[object].position(), filters[object].velocity(), modAngle(filters[object].orientation()));
}

void Tracker::filter(Object::Type object, const Point& position, filter_t heading) {
  filters[object].setTimeDelta(delta_t);
  filters[object].predict();

  Vector velocity((position - prev_state_[object].pos) / delta_t);
  // unfiltered heading
  //filters[object].correct(position, velocity);

  //state_[object].set(filters[object].position(), filters[object].velocity(), heading);

  // filtered heading
  //filters[object].correct(position, velocity, (isUnknown<filter_t>(heading) ? prev_state_[object].orientation : heading));

  //state_[object].set(filters[object].position(), filters[object].velocity(), filters[object].orientation());
  
  // wrapped filtered heading
  filter_t diff;
  if (isUnknown<filter_t>(heading)) {
    diff = 0.0f;
  }
  else if (std::signbit(heading) != std::signbit(prev_state_[object].orientation)
           && fabs(heading) > M_PI/2) { // check if wrapping at -x axis
      diff = heading - prev_state_[object].orientation;
      diff += std::signbit(heading) ? 2*M_PI : -2*M_PI;
  }
  else {
    diff = heading - prev_state_[object].orientation;
  }

  filters[object].correct(position.to<Vector2<filter_t>>(), velocity.to<Vector2<filter_t>>(), static_cast<filter_t>(filters[object].orientation()) + diff);

  state_[object].set(filters[object].position(), filters[object].velocity(), modAngle(filters[object].orientation()));
}

void Tracker::locateRobot(Object::Type robot) {
  //assert(robot >= Object::Robot1 && robot <= Object::Opponent2);

  Pixel big;
  Pixel small;
  
  if (locateObjects(images_[robot], processor_.objectFilter(robot), RobotFinder(&big, &small))) {
    if (small.x >= 0) {
      filter(robot, processor_.toPosition(midpoint(small, big)), heading(big, small));
    }
    else { // if only found the big blob, just return the position w/o orientation
      filter(robot, processor_.toPosition(big));
    }
  }
  else {
    filter(robot);
  }
}

//
// Functions for multi-threading
//

void Tracker::initializeThreads() {
  for (int i = 0; i < Object::COUNT-1; ++i) {
    sem_init(&start_[i], 0, 0);
  }

  sem_init(&done_, 0, 0);

  std::thread(threadLauncher, this, Object::Robot1).detach();
  //std::thread(threadLauncher, this, Object::Robot2).detach();
  std::thread(threadLauncher, this, Object::Opponent1).detach();
  //std::thread(threadLauncher, this, Object::Opponent2).detach();

  std::cout << "Launched " << sizeof(TO_TRACK) / sizeof(Object::Type) << " additional threads." << std::endl;
}

void Tracker::threadLauncher(Tracker* tracker, Object::Type type) {
  tracker->objectFinder(type);
}

void Tracker::objectFinder(Object::Type object) {
  while (true) {
    sem_wait(&start_[object]);

    images_[object] = processor_.filterBall(hsv_);
    locateRobot(object);

    sem_post(&done_);
  }
}

void Tracker::startProcessing() {
  sem_post(&start_[0]);
  sem_post(&start_[1]);
  sem_post(&start_[2]);
  sem_post(&start_[3]);
}

void Tracker::waitUntilDone() {
  sem_wait(&done_);
  sem_wait(&done_);
  sem_wait(&done_);
  sem_wait(&done_);
}

const GameState& Tracker::updateMulti(const Mat& frame) {
  prev_state_ = state_;

  // prepare the frame
  Mat undistorted = processor_.undistort(frame);
  Mat cropped = processor_.crop(undistorted);
  hsv_ = processor_.preprocess(cropped);

  delta_t = timeSinceLastCall<filter_t>();

  startProcessing();

  images_[Object::Ball] = processor_.filterBall(hsv_);
  locateBall();

  waitUntilDone();

  // if using the calibration GUI, draw the objects
  if (cal) {
    cal->drawGameState(cropped, state_);

    for (Object::Type obj : TO_TRACK) {

      cal->displayProcessed(images_[obj], obj);
    }

    cal->displayProcessed(images_[Object::Ball], Object::Ball);

    cal->displayOriginal(undistorted);
    cal->displayCropped(cropped);
    cal->waitForInput();
  }

  return state_;
}
