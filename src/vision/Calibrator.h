#ifndef CALIBRATOR_H
#define CALIBRATOR_H

#include <opencv2/opencv.hpp>
#include <limits>

#include "ColorFilter.h"
#include "GameState.h"
#include "Vector2.h"
#include "ImageProcessor.h"

using cv::Mat;

/**
 * Provide a user interface for calibrating the tracking.
 */
class Calibrator {
private:
  // window names
  constexpr static const char* MAIN = "Original Image";
  constexpr static const char* CROPPED = "Cropped Image";
  constexpr static const char* FILTER = "Configure Filter";
  constexpr static const char* PROCESSED = "Processed Image";

  constexpr static const char* LABELS[] = {
    "Robot 1",
    //"Robot 2",
    "Opponent Robot 1",
    //"Opponent Robot 2",
    "Ball",
  };

  ImageProcessor* processor_;
  int line_type_; // basically, whether or not to use antialiasing
  Object::Type current_filter_;

  // keep track of mouse clicks
  Pixel corners_[4]; 
  int cur_corner_;

  Pixel position_;

  // an area measurement
  bool circle_area_;
  Pixel area_start_, area_end_;
  double radius_, area_;

  // how much blur to use for all filters
  int blur_setting_;

  // the hue settings
  int hue_min_;
  int hue_max_;

  /** Handle changing the hue */
  static void changeHue(int, void* self);

  /** Populate the configuration window with trackbars. */
  void createTrackbars();

  /** Draw the selected area on the provided frame  */
  void drawArea(const Mat& frame) const;

  /** Handle measuring areas with the mouse  */
  void mouseMeasureArea(int event, int x, int y, int flags);

  /** Handle measuring areas with the mouse  */
  void mouseMarkCorners(int event, int x, int y, int flags);

  /** Handle converting from global to field coordinates */
  void mouseGetFieldCoordinate(int event, int x, int y, int flags);

  /** Process a mouse click. */
  static void handleMainMouseClick(int event, int x, int y, int flags, void* userdata);

  /** Process a mouse click in the cropped window. */
  static void handleAreaOrCoordinateMouseClick(int event, int x, int y, int flags, void* userdata);
  
  /** Limit a value to between a minimum and maximum. */
  template<typename T>
  inline static T limit(T value, T min, T max = std::numeric_limits<T>::max()) {
    if (value <= min) return min;
    if (value >= max) return max;
    else              return value;
  }

  /** Determine whether a string means yes */
  inline static bool isYes(const std::string& resp) { 
    return (resp == "y" || resp == "Y" || resp == "yes" || resp == "Yes");
  }

  /** Determine whether a string means yes */
  inline static bool isYes(char resp) { 
    return (resp == 'y' || resp == 'Y');
  }

  /** Find out which corner is which (convex hull-ish) */
  void sortCorners(Pixel sorted[4]) const;

  /**
   * Draw a crosshair on a frame.
   * @param pixel the location of the crosshair
   * @param frame the frame on which to draw the crosshair
   * @param message the message (can be any <<-able type)
   * @param color the color
   * @param thickness the thickness of the lines to draw
   */
  template<typename T = std::string>
  void drawCrosshair(const Mat& frame, const Pixel& pixel, const T& message = {}, const Scalar& color = Scalar(255, 255, 255), int thickness = 1) const {
    using cv::Point2i;

    static const int SIZE = 25;
    static const int RADIUS = 20;
    
    Point2i cvPoint(pixel.to<Point2i>());

    // Draw a circle around the point
    circle(frame, cvPoint, RADIUS, color, thickness, line_type_);

    line(frame, cvPoint, Point2i(pixel.x, limit(pixel.y - SIZE, 0)), color, thickness, line_type_);
    line(frame, cvPoint, Point2i(pixel.x, limit(pixel.y + SIZE, 0, frame.rows-1)), color, thickness, line_type_);
    line(frame, cvPoint, Point2i(limit(pixel.x - SIZE, 0), pixel.y), color, thickness, line_type_);
    line(frame, cvPoint, Point2i(limit(pixel.x + SIZE, 0, frame.cols-1),  pixel.y), color, thickness, line_type_);
    
    putText(frame, pixel.toString(), Point2i(pixel.x, pixel.y + SIZE + 8 + thickness), 1, 1, color, thickness, line_type_);

    std::ostringstream str;
    str << message;
    std::string msg(str.str().empty() ? " " : str.str()); // putText crashes if msg is blank!

    putText(frame, msg, Point2i(pixel.x, pixel.y - SIZE - 5), 1, 1, color, thickness, line_type_);
  }

  /** Draw the goal posts */
  void drawGoals(const Mat& frame) const;

public:
  /**
   * @brief Construct an image calibrator.
   * This provides a GUI to calibrate video/images in real time.
   *
   * @param processor the image processor object to use
   */
  Calibrator(ImageProcessor* processor, bool antialiasing = true);

  /**
   * @brief Display an image in the original image window
   *
   * @param frame the image to display
   */
  void displayOriginal(const Mat& frame) const;

  /**
   * @brief Display an image in the original image window
   *
   * @param frame the image to display
   */
  void displayCropped(const Mat& frame) const;

  /**
   * @brief Display an image in the original image window
   * Only the processed image for the current filter will be displayed.
   *
   * @param frame the image to display
   * @param current the filter the frame belongs to
   */
  void displayProcessed(const Mat& frame, Object::Type current) const;

  /**
   * Track an object's position and orientation (if it is known)
   */
  void drawObject(const Mat& frame, const Object& obj, const std::string& message, const cv::Scalar& circle_color = Scalar(200,200,200), int thickness = 1, int heading_length = 60) const;

  /**
   * Track a whole GameState.
   */
  void drawGameState(const Mat& frame, const GameState& state) const {
    for (int obj = 0; obj < Object::COUNT; ++obj) {
      drawObject(frame, state[obj], LABELS[obj]);
    }
  }

  /**
   * Wait for input and process input from the user.
   *
   * @param ms how long to wait, in ms (1 ms should be enough)
   */
  void waitForInput(int ms = 1);

  /**
   * Print the controls for the calibrator.
   */
  void help(std::ostream& os = std::cout) const;
};

#endif
