#include "Field.h"

#include <fstream>
#include <set>
#include <cmath>

Field::Field(const char* config_file, FieldInfo::Team side, int width, int height, int border)
    : config_file_(config_file), side_(side), border_(border),
      max_width_(width), max_height_(height) {
  std::ifstream file(config_file_);

  if (!file.is_open()) {
    std::cerr << "WARNING: Failed to open field config file " << config_file_ << '.' << std::endl;
  }
  else for (int i = 0; i < 4; ++i) {
    file >> corners_[i].x >> corners_[i].y;
  }

  if (!validCorners()) {
    std::cerr << "Invalid corners were used. Resetting to image size." << std::endl;
    resetCorners();
  }

  calculateBounds();
}

void Field::resetCorners() {
    corners_[0] = Pixel(0, 0);
    corners_[1] = Pixel(max_width_-1, 0);
    corners_[2] = Pixel(0, max_height_-1);
    corners_[3] = Pixel(max_width_-1, max_height_-1);
    border_ = 0;
}

bool Field::validCorners() const {
  return validCorners(corners_);
}

bool Field::validCorners(const Pixel corners[4]) const {
  for (int i = 0; i < 4; ++i) {
    if (!corners[i].inRange(0, max_width_, 0, max_height_)) {
      return false;
    }
  }

  return true;
}

void Field::calculateBounds() {
  topLeft_.x  = min(corners_[0].x, corners_[2].x);
  topLeft_.y  = min(corners_[0].y, corners_[1].y);
  botRight_.x = max(corners_[1].x, corners_[3].x);
  botRight_.y = max(corners_[2].y, corners_[3].y);

  //p_ml_ = static_cast<float>(corners_[2].y - corners_[0].y) / 2.0f + corners_[0].y;// - upmost;
  //p_mr_ = static_cast<float>(corners_[3].y - corners_[1].y) / 2.0f + corners_[1].y;// - upmost;
  //p_mt_ = static_cast<float>(corners_[1].x - corners_[0].x) / 2.0f + corners_[0].x;// - leftmost;
  //p_mb_ = static_cast<float>(corners_[3].x - corners_[2].x) / 2.0f + corners_[2].x;// - leftmost;

  center_ = (corners_[0] + corners_[1] + corners_[2] + corners_[3]).to<Point>() / 4.0 - topLeft_.to<Point>();

  scale_ = (botRight_ - topLeft_).to<Vector>();
  scale_.x = FieldInfo::WIDTH / scale_.x; // TODO: probably shouldn't have different x and y scales
  scale_.y = FieldInfo::HEIGHT / scale_.y;
  //scale_.y = scale_.x; // TODO: could do this to get same scale, or maybe average both?
}

bool Field::storeCorners(const Pixel new_corners[4]) {
  // make sure the corners are within bounds and reset if not
  if (!validCorners(new_corners)) return false;

  // sort the pixels by x coordinate
  std::multiset<Pixel> pixels;

  for (int i = 0; i < 4; ++i) {
    pixels.emplace(new_corners[i]);
  }

  // sort the leftmost and rightmost two pixels by y coordinate
  auto it = pixels.begin();
  const Pixel& left1(*it++);
  const Pixel& left2(*it++);

  int index = static_cast<int>(left1.y < left2.y);
  corners_[2 * !index] = left1;
  corners_[2 * index] = left2;

  const Pixel& right1(*it++);
  const Pixel& right2(*it);

  index = static_cast<int>(right1.y < right2.y);
  corners_[2 * !index + 1] = right1;
  corners_[2 * index  + 1] = right2;

  return true;
}

//Point Field::toPosition(int pixel_x, int pixel_y) const {
//  // TODO: Figure out how to work with side_... make sure it won't confuse the robots
//  // This is a rough approximation, not exact
//  float y_ratio_l = static_cast<float>(pixel_y - p_ml_) / static_cast<float>(corners_[2].y - p_ml_);
//  float y_ratio_r = static_cast<float>(pixel_y - p_mr_) / static_cast<float>(corners_[3].y - p_mr_);
//  float y = side_ * (y_ratio_l + y_ratio_r) / 2.0f;
//
//  float x_ratio_t = static_cast<float>(pixel_x - p_mt_) / static_cast<float>(corners_[1].x - p_mt_);
//  float x_ratio_b = static_cast<float>(pixel_x - p_mb_) / static_cast<float>(corners_[3].x - p_mb_);
//  float x = side_ * (x_ratio_t + x_ratio_b) / 2.0f;
//
//  return Point(x * FieldInfo::WIDTH/2, y * FieldInfo::HEIGHT/2);
//}

Point Field::toPosition(int pixel_x, int pixel_y) const {
  float center_x = pixel_x - center_.x - border_;
  float center_y = center_.y - pixel_y + border_; // reverse the y so it's positive on top

  return side_ * Point(center_x * scale_.x, center_y * scale_.y);
}

Pixel Field::toPixel(float x, float y) const {
  int pixel_x = center_.x + side_ * static_cast<int>(round(x/scale_.x)) + border_;
  int pixel_y = center_.y - side_ * static_cast<int>(round(y/scale_.y)) + border_;

  return Pixel(pixel_x, pixel_y);
}

void Field::setBorder(int new_border) {
  border_ = new_border;
}

void Field::save() const {
  std::ofstream file(config_file_);

  for (int i = 0; i < 4; ++i) {
    file << corners_[i].x << ' ' << corners_[i].y << '\n';
  }
}

bool Field::save(const Pixel new_corners[4]) {
  if (storeCorners(new_corners)) {
    calculateBounds();
    save();
    return true;
  }
  else {
    return false;
  }
}
