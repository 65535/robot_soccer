#include "Calibrator.h"

#include <cstdlib>
#include <ros/ros.h>
#include <robot_soccer/Velocity.h>

namespace {
Vector command;
ros::NodeHandle* nh;
ros::Subscriber sub;

void updateVelocity(const robot_soccer::Velocity::ConstPtr& msg) {
  command.x = msg->x;
  command.y = msg->y;
}

}

Calibrator::Calibrator(ImageProcessor* processor, bool antialiasing)
    : processor_(processor), line_type_(antialiasing ? cv::LINE_AA : cv::LINE_8),
      current_filter_(Object::Robot1),
      cur_corner_(0), circle_area_(true), blur_setting_(processor->blurSize()/2),
      hue_min_(ColorFilter::HUE_OFFSET), hue_max_(ColorFilter::HUE_OFFSET + 179) {
  const int RESIZE_BEHAVIOR = cv::WINDOW_AUTOSIZE;
  //const int RESIZE_BEHAVIOR = cv::WINDOW_NORMAL;

  cv::namedWindow(MAIN,      RESIZE_BEHAVIOR);
  cv::namedWindow(CROPPED,   RESIZE_BEHAVIOR);
  cv::namedWindow(PROCESSED, RESIZE_BEHAVIOR);

  createTrackbars();
  cv::setMouseCallback(MAIN, handleMainMouseClick, this);
  cv::setMouseCallback(CROPPED, handleAreaOrCoordinateMouseClick, this);
  cv::setMouseCallback(PROCESSED, handleAreaOrCoordinateMouseClick, this);

  help();

  nh = new ros::NodeHandle();
  sub = nh->subscribe("robot1", 1, updateVelocity); // listen to commands
}

constexpr const char* Calibrator::LABELS[];

void Calibrator::changeHue(int, void* calibrator) {
  Calibrator& self(*static_cast<Calibrator*>(calibrator));
  ColorFilter& filter(self.processor_->objectFilter(self.current_filter_));

  filter.setShiftedHue(self.hue_min_, self.hue_max_);
}

void Calibrator::createTrackbars() {
  static std::string window_name(LABELS[current_filter_]);

  ColorFilter& filter(processor_->objectFilter(current_filter_));

  // initialize local copy of hue_min_ and hue_max_ values
  hue_min_= filter.shiftedMinHue();
  hue_max_= filter.shiftedMaxHue();

  cv::destroyWindow(window_name);
  window_name = LABELS[current_filter_];
  cv::namedWindow(window_name, cv::WINDOW_NORMAL);

  std::cout << "Current filter: " << LABELS[current_filter_] << " (" << filter.sourceFilename() << ')' << std::endl;

  cv::createTrackbar("Minimum Hue", window_name, &hue_min_, 359, changeHue, this);
  cv::createTrackbar("Maximum Hue", window_name, &hue_max_, 359, changeHue, this);

  cv::createTrackbar("Minimum Saturation", window_name, &filter.s.min, filter.s.maxValue(), nullptr);
  cv::createTrackbar("Maximum Saturation", window_name, &filter.s.max, filter.s.maxValue(), nullptr);
  cv::createTrackbar("Minimum Value", window_name, &filter.v.min, filter.v.maxValue(), nullptr);
  cv::createTrackbar("Maximum Value", window_name, &filter.v.max, filter.v.maxValue(), nullptr);
  cv::createTrackbar("Erode / Dilate", window_name, &filter.morph_size, 40, nullptr);

  auto changeBlur = [](int value, void* processor) {
    static_cast<ImageProcessor*>(processor)->setBlurSize(2*value+1);
  };

  cv::createTrackbar("Blur (for all filters)", window_name, &blur_setting_, 50, changeBlur, processor_);
}

void Calibrator::drawArea(const Mat& frame) const {
  if (area_end_.x >= 0) {
    const cv::Scalar color(128, 255, 255);

    if (circle_area_) {
      circle(frame, area_start_.to<cv::Point2i>(), radius_, color, 1, line_type_);
    }
    else {
      Pixel diff = area_end_ - area_start_;
      rectangle(frame, (area_start_ - diff).to<cv::Point2i>(), area_end_.to<cv::Point2i>(), color, 1, line_type_);
    }
    putText(frame, std::to_string(area_), cv::Point2i(area_start_.x, area_start_.y - radius_ - 5), 1, 1, color, 1, line_type_);
  }
}

void Calibrator::mouseMeasureArea(int event, int x, int y, int flags) {
  static bool holding = false;
  static Pixel start_click;

  switch (event) {
  case cv::EVENT_MOUSEMOVE:
    if (holding) {
      if (start_click.x >= 0) {
        Pixel click(x, y);
        Pixel delta(click - start_click);
        area_end_ += delta;
        area_start_ += delta;

        start_click = click;
      }
      else {
        area_end_.x = x;
        area_end_.y = y;

        radius_ = Pixel::distance(area_end_, area_start_);

        if (circle_area_) {
          area_ = M_PI * radius_ * radius_;
        }
        else { // area of the rectangle
          Pixel diff = area_end_ - area_start_;
          area_ = abs(4 * diff.x * diff.y);
        }
      }
    }
    break;
  case cv::EVENT_LBUTTONDOWN: {
    static const int RESIZE_RADIUS = 3;

    holding = true;
    Pixel click(x, y);

    double distance = Pixel::distance(click, area_start_);
    if (distance < radius_ - RESIZE_RADIUS) {
      start_click = click;
    }
    else if (distance > radius_ + RESIZE_RADIUS) {
      area_start_ = click;
    }
    break;
  }
  case cv::EVENT_LBUTTONUP:
    holding = false;
    start_click = Pixel();
    break;
  default:
    area_start_ = Pixel();
    area_end_ = Pixel();
    start_click = Pixel();
    break;
  }
}

void Calibrator::mouseMarkCorners(int event, int x, int y, int flags) {
  static bool holding = false;

  switch (event) {
  case cv::EVENT_LBUTTONDOWN: {
    static const float RANGE = 35.0f;
    holding = true;

    // find which corner was pressed (or move to a new one)
    Pixel click(x, y);
    int i;
    for (i = 0; i < 4; ++i) {
      if (corners_[i].x < 0 || Pixel::distance(click, corners_[i]) < RANGE) {
        cur_corner_ = i;
        goto found_corner;
      }
    }
    cur_corner_ = (cur_corner_ + 1) % 4;

    found_corner:
    Point position(processor_->toPosition(processor_->toCroppedCoordinates(x, y)));

    std::cout << position << std::endl;

    // FALL THROUGH
  }
  case cv::EVENT_MOUSEMOVE:
    if (holding) {
      corners_[cur_corner_].x = x;
      corners_[cur_corner_].y = y;
    }
    break;
  case cv::EVENT_RBUTTONDOWN:
  case cv::EVENT_MBUTTONDOWN:
    // reset after any other click
    for (int i = 0; i < 4; ++i) {
      corners_[i] = Pixel();
      cur_corner_ = 0;
    }
  case cv::EVENT_LBUTTONUP:
    holding = false;
    break;
  }
}

void Calibrator::mouseGetFieldCoordinate(int event, int x, int y, int flags) {
  static bool holding = false;

  switch (event) {
  case cv::EVENT_LBUTTONDOWN:
    holding = true;
  case cv::EVENT_MOUSEMOVE:
    if (holding) {
      position_.x = x;
      position_.y = y;
    }
    break;
  case cv::EVENT_RBUTTONDOWN:
  case cv::EVENT_MBUTTONDOWN:
    position_ = Pixel();
  case cv::EVENT_LBUTTONUP:
    holding = false;
    break;
  }
}

void Calibrator::handleMainMouseClick(int event, int x, int y, int flags, void* userdata) {
  Calibrator& self(*static_cast<Calibrator*>(userdata));

  self.mouseMarkCorners(event, x, y, flags);
}

void Calibrator::handleAreaOrCoordinateMouseClick(int event, int x, int y, int flags, void* userdata) {
  Calibrator& self(*static_cast<Calibrator*>(userdata));
  if (flags & cv::EVENT_FLAG_SHIFTKEY) {
    self.mouseMeasureArea(event, x, y, flags);
  }
  else {
    self.mouseGetFieldCoordinate(event, x, y, flags);
  }
}

void Calibrator::drawGoals(const Mat& frame) const {
  static const Point offset(0.05f, 0.0f);
  static const cv::Scalar color(0, 0, 0, 128);
  static const int thickness = 2;

  using namespace cv;

  line(
    frame,
    processor_->toPixel(FieldInfo::GOAL_TOP).to<Point2i>(),
    processor_->toPixel(FieldInfo::GOAL_TOP - offset).to<Point2i>(),
    color,
    thickness,
    line_type_
  );
  
  line(
    frame,
    processor_->toPixel(FieldInfo::GOAL_BOT).to<Point2i>(),
    processor_->toPixel(FieldInfo::GOAL_BOT - offset).to<Point2i>(),
    color,
    thickness,
    line_type_
  );

  line(
    frame,
    processor_->toPixel(FieldInfo::OUR_GOAL_BOT).to<Point2i>(),
    processor_->toPixel(FieldInfo::OUR_GOAL_BOT + offset).to<Point2i>(),
    color,
    thickness,
    line_type_
  );

  line(
    frame,
    processor_->toPixel(FieldInfo::OUR_GOAL_TOP).to<Point2i>(),
    processor_->toPixel(FieldInfo::OUR_GOAL_TOP + offset).to<Point2i>(),
    color,
    thickness,
    line_type_
  );
}

void Calibrator::sortCorners(Pixel sorted[4]) const {
  // I copied this code from Field.cpp. Oh well.
  // sort the pixels by x coordinate first
  std::multiset<Pixel> pixels;

  for (int i = 0; i < 4; ++i) {
    pixels.emplace(corners_[i]);
  }

  // sort the leftmost and rightmost two pixels by y coordinate
  auto it = pixels.begin();
  const Pixel& left1(*it++);
  const Pixel& left2(*it++);

  int index = static_cast<int>(left1.y < left2.y);
  sorted[2 * !index] = left1;
  sorted[2 * index] = left2;

  const Pixel& right1(*it++);
  const Pixel& right2(*it);

  index = static_cast<int>(right1.y < right2.y);
  sorted[2 * !index + 1] = right1;
  sorted[2 * index  + 1] = right2;
}

void Calibrator::displayOriginal(const Mat& frame) const {
  // Draw the last 4 mouse clicks
  for (int i = 0; i < 4; ++i) {
    if (corners_[i].x >= 0) drawCrosshair(frame, corners_[i], "", Scalar(180, 180, 180));
  }

  // Draw lines between the mouse clicks to show the field boundaries
  Pixel sorted[4];
  sortCorners(sorted);

  static const int ORDER[4] = {0, 1, 3, 2};

  for (int i = 1; i < 4+1; ++i) {
    const Pixel& corner0(sorted[ORDER[i-1]]);
    const Pixel& corner1(sorted[ORDER[i%4]]);

    if (corner0.inRange(0, frame.cols, 0, frame.rows) && corner1.x >= 0) {
      line(frame, corner0.to<cv::Point2i>(), corner1.to<cv::Point2i>(), Scalar(0,0,255), 1, line_type_);
    }
  }

  imshow(MAIN, frame);
}

void Calibrator::displayCropped(const Mat& frame) const { 
  if (position_.x >= 0) drawCrosshair(frame, position_, processor_->toPosition(position_), Scalar(255, 255, 255));
  
  drawArea(frame);
  drawGoals(frame);

  // draw the center
  circle(frame, processor_->toPixel(FieldInfo::CENTER).to<cv::Point2i>(), 1, Scalar(0,0,0), 1, line_type_);

  imshow(CROPPED, frame);
}

void Calibrator::displayProcessed(const Mat& frame, Object::Type current) const {
  if (current == current_filter_) {
    drawArea(frame);
    imshow(PROCESSED, frame);
  }
}

void Calibrator::drawObject(const Mat& frame, const Object& obj, const std::string& message, const cv::Scalar& circle_color, int thickness, int heading_length) const {
  // draw the velocity
  static const float VEL_SCALE = 1;

  if (!isUnknown(obj.pos.x)) {
    Pixel center(processor_->toPixel(obj.pos));
    drawCrosshair(frame, center, message, circle_color);

    // draw the orientation
    if (!isUnknown(obj.orientation)) {
      Pixel dir(heading_length * cos(obj.orientation), -heading_length * sin(obj.orientation));
      cv::arrowedLine(frame, center.to<cv::Point2i>(), (center + processor_->side()*dir).to<cv::Point2i>(), Scalar(255, 0, 255), thickness, line_type_);
    }

    //std::cout << obj.vel << std::endl;

    if (!isUnknown(obj.vel.x)) {
      cv::arrowedLine(frame, center.to<cv::Point2i>(), processor_->toPixel(obj.pos + VEL_SCALE * obj.vel).to<cv::Point2i>(), Scalar(255, 255, 0), thickness, line_type_);
    }

    // draw the velocity command
    if (message[0] == 'R') {
      ros::spinOnce();
      if (!isUnknown(command.x)) {
        cv::arrowedLine(frame, center.to<cv::Point2i>(), processor_->toPixel(obj.pos + 0.25*VEL_SCALE * command).to<cv::Point2i>(), Scalar(0, 255, 0), 2*thickness, line_type_);
      }
    }
  }
  else if (message[0] == 'R') { // only draw if drawing Robot
      ros::spinOnce();
      if (!isUnknown(command.x)) {
        cv::arrowedLine(frame, processor_->toPixel(FieldInfo::CENTER).to<cv::Point2i>(), processor_->toPixel(obj.pos + 0.25*VEL_SCALE * command).to<cv::Point2i>(), Scalar(0, 255, 0), 2*thickness, line_type_);
      }
  }
}

void Calibrator::waitForInput(int ms) {
  // Check for keypresses to control saving
  char key = static_cast<char>(cv::waitKey(ms));

  if (key >= '1' && key < (Object::COUNT + '1')) {
    current_filter_ = static_cast<Object::Type>(key - '1');
    createTrackbars();
  }
  else switch (key) {
    // toggle area measurement tool (circle or rectangle)
    case 'a':
    case 'A':
      circle_area_ = !circle_area_;
      area_start_ = area_end_ = Pixel();
      std::cout << "Now measuring areas with a " << (circle_area_ ? "circle." : "rectangle.") << std::endl;
      break;
    // save blur setting (gets saved by saving filter settings too)
    case 'b':
    case 'B': 
      std::cout << "Saved the blur setting." << std::endl;
      processor_->saveBlur();
      break;
    // Save current color filter
    case 'c':
    case 'C': {
      ColorFilter& filter(processor_->objectFilter(current_filter_));

      std::cout << "Saving current filter for " << LABELS[current_filter_] << '.' << std::endl;
      std::cout << "Filename? (press enter to use current -- \"" << filter.sourceFilename() << "\")" << std::endl;

      std::string file;
      getline(std::cin, file);
      std::cout << std::endl;

      processor_->saveFilter(current_filter_, file);
      std::cout << "Saved to \"" << filter.sourceFilename() << "\"." << std::endl;
   
      break;
    }
    // save field corners
    case 'f':
    case 'F': 
      // check that the corners are all defined
      if (corners_[0].x >= 0 && corners_[1].x >= 0 && corners_[2].x >= 0 && corners_[3].x >= 0) {
        std::cout << "Save field corners? (press y for yes)" << std::endl;
        if (isYes(cv::waitKey(0))) {
          if (processor_->saveField(corners_)) {
            std::cout << "Saved field corners." << std::endl;
          }
          else {
            std::cout << "The corners were invalid (off screen) and not saved." << std::endl;
          }
        }
      }
      else std::cout << "You must define all four corners to save the field." << std::endl;
      break;
    case ' ': // Pause
      std::cout << "Paused. Press space to continue." << std::endl;
      while (static_cast<char>(cv::waitKey(0)) != ' ');
      break;
    // help
    case 'h':
    case 'H':
      help();
      break;
    // save area
    case 'm': {
      processor_->objectFilter(current_filter_).min_size = area_;
      std::cout << "Updated minimum size. Press c to save the color filter to disk." << std::endl;
      break;
    }
    case 'M': {
      processor_->objectFilter(current_filter_).max_size = area_;
      std::cout << "Updated maximum size. Press c to save the color filter to disk." << std::endl;
      break;
    }
    // quit
    case '\x1B': // Escape key
    case 'q':
    case 'Q':
      exit(0);
      return;
    // reset
    case 'r':
    case 'R':
      processor_->objectFilter(current_filter_).reset();
      createTrackbars();
  }
}

void Calibrator::help(std::ostream& os) const {
  os << "Calibrator controls:\n"
        "\t1-5\tswitch which object the filter is for\n"
        "\ta\tchange whether the area measurement tool uses a circle or rectangle\n"
        "\tb\tsave the blur setting (applies to all filters)\n"
        "\tc\tsave the color filter settings\n"
        "\tf\tsave the field dimensions\n"
        "\th\tview this help information\n"
        "\tm\tupdate marked area as the minimum area for the current object (does not save to disk)\n"
        "\tM\tupdate marked area as the maximum area for the current object (does not save to disk)\n"
        "\tq\tquit\n"
        "\tr\treset the current filter (does not save changes to disk)\n"
        "\n"
        "Click on the full frame to set the corners of the field.\n"
        "Right click to clear the selection. Press f so save when ready.\n"
        "\n"
        "Shift click on the cropped frame to set the minimum or maximum area for the current object.\n"
        "Hold shift and right click to clear the area.\n"
        "\n"
        "Left click on the cropped frame to view the pixel value and field coordinate.\n"
        "\n"
        "NOTE: Gaussian blur and erode/dilate add substantially to the latency.\n"
        "      Turning the blur or erode/dilate to zero turns them off.\n"
     << std::endl;
}
