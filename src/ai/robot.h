/**
* @file robot.h
* @brief This is a %Robot model for the AI control
* @author Jonathan Frahm
* @version 1.0
* @date 2015-02-16
*/
#ifndef ROBOT_H
#define ROBOT_H

#include <ros/ros.h>
#include "../vision/FieldInfo.h"
#include "object_info.h"
#include <robot_soccer/Object.h>
#include <robot_soccer/GameState.h>
#include <robot_soccer/Velocity.h>

/**
 * @brief A robot model for the AI control
 */
class Robot {
	public:
		Robot();
		
		Robot(const char *topic_name, ros::NodeHandle* handle);
		
		void stop();
		
		bool atPos(Vector pos);

		void playRushGoal();

		void playDefendGoal();

		void skillMoveToPoint(Vector location);

		void skillMoveToPoint(Vector location, Vector direction);

		void skillGetBehindBall(Vector waypoint);

		void skillSpin();

		void skillGoToCenter();

		void updateState(const robot_soccer::GameState::ConstPtr& state);
		
		void aggressiveStateMachine();

	private:

		bool checkSide(Point p, Point l1, Point l2);
		bool onField(Point p);
	
		Vector rotate(Vector p, Vector origin, bool cw);

		float distanceToLine(Point p, Point l1, Point l2);

		bool behindBall(Point waypoint);

		bool okayToApproach(Point waypoint);
		
		void updateState(ObjectInfo* obj, robot_soccer::Object msg);
		
		float getAngleToFacePoint(Vector direction);

		float getFlatSide(float angle);
		
		/**
		 * @brief ROS publisher to send messages to robot node
		 */
		ros::Publisher publisher;
		
		/**
		 * @brief Publish to the robot's ROS topic
		 */
		void publish();

		/**
		* @brief Boolean that determines whether it is okay to send commands
		*/

		ObjectInfo self;
		ObjectInfo opp;
		ObjectInfo ball;
};

#endif
