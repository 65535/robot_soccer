/**
* @file controller.cpp
* @brief The AI controller for a robot soccer team. The 
* controller makes use of ROS to communicate velocity
* commands to robots.
*
* @author Jonathan Frahm
* @version 1.0
* @date 2015-02-01
*/
#include "boost/program_options.hpp"
#include "controller.h"
#include "../vision/Vector2.h"
#include <math.h>
#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>

#define RETURN_TO_START 0
#define CENTER 1
#define DEFEND_GOAL 2
#define RUSH_GOAL 3
#define TEST_START 4 
#define TEST_FINISH 5
#define NUM_PLAYS 3

namespace ai_node {
	bool DEBUG;
 	std::string STATE_TOPIC = "simulator_state";
	std::string PLAY = "center";
	int mode = CENTER;

	std::string allowed_plays[] = {
		"center",
		"defend-goal",
		"rush-goal"
	};

	Controller ai;

	Vector START_POS(-0.5f, 0);
	Vector TEST_START_POS(-1, 0.869);
	Vector TEST_FINISH_POS(1, 0.869);
 
  //Exit codes
  const size_t SUCCESS = 0;
  const size_t ERROR_IN_COMMAND_LINE = 1;
};

using namespace ai_node;

void pause_game(int s){
  // TODO: Build and publish a stop velocity command;
	std::cout << "Returning to start position" << std::endl;
	int prev_play = mode;
	mode = RETURN_TO_START;
	ros::Rate loop_rate(10);
	int successes = 0;
	while (successes < 5){
		if (ai.atStartingPosition()){
			successes++;
		}
		std::cout << "." << std::flush;
		ros::spinOnce();
		loop_rate.sleep();
	}
	ai.stop();
	while (true){
		std::cout << "Ready. Resume play? [y/n] " << std::flush;
		char response;
		std::cin >> response;
		if (response == 'y'){
			mode = prev_play;
			return;
		}
		else if (response == 'n'){
			std::cout << "Exiting" << std::endl;
			ros::shutdown();
		}
		else
			std::cout << "Invalid response: " << response << std::endl;	
	}
}

/**
* @brief Handler for game state messages
*
* @param state Message contain game state info
*/
void Controller::processGameState(const robot_soccer::GameState::ConstPtr& state) {
	parseState(state);
	switch (mode) {
		case RETURN_TO_START:
			us.skillMoveToPoint(START_POS, FieldInfo::GOAL_CENTER);
		break;
		case CENTER:
			goToCenter();
		break;
		case DEFEND_GOAL:
			defendGoal();
		break;
		case RUSH_GOAL:
			rushGoal();
		break;
		case TEST_START:
			us.skillMoveToPoint(TEST_START_POS);
		break;
		case TEST_FINISH:
			us.skillMoveToPoint(TEST_FINISH_POS);
		break;
		default:
			std::cout << "Invalid mode of operation\n";	
	}
}

/**
* @brief Run the AI
*
* @param argc Number of command line arguments
* @param argv Command line arguments
*/
int Controller::run(int argc, char** argv){
	namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()
    ("help,h", "Print help messages")
    ("state-topic,s",po::value<std::string>(&STATE_TOPIC),"Name of ROS topic providing state messages")
    ("play,p",po::value<std::string>(&PLAY),"Which play should be run")
    ("debug,d", "print debug information")
    ("test,t", "test lateral movement precision");

  po::variables_map vm; 
  try{
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")){
      std::cout << "AI Control" << std::endl
                << "Usage: rosrun robot_soccer ai_node [options]" << std::endl
                << desc << std::endl;
      return SUCCESS;
    }   
    DEBUG = vm.count("debug");
		if (vm.count("play")){
			if (!isValidPlay(PLAY)){
				std::cout << "ERROR: Invalid play.Options include:" << std::endl;
				for (int i = 0; i < NUM_PLAYS; i++){
					std::cout << "\t" << allowed_plays[i] << std::endl;
				}
				return ERROR_IN_COMMAND_LINE;
			}
		}
  }
  catch(po::error& e){ 
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl
              << desc << std::endl;
    return ERROR_IN_COMMAND_LINE;
  }

	ros::init(argc, argv, "ai", ros::init_options::NoSigintHandler);

	signal(SIGINT, pause_game);
	
	// This object is the main access point to communications with
	// the ROS system
	ros::NodeHandle handle;

	// Initialize robot models 
	us = Robot("robot1", &handle);

	gameState = handle.subscribe(STATE_TOPIC, 1, &Controller::processGameState, this);

	if (vm.count("test")){
		mode = TEST_START;
		while(!atTestStart()){
			ros::spinOnce();
		}
		stop();
		char c;
		while(c != 'y'){
			std::cout << "Ready? [y/n] ";
			std::cin >> c;
		}
		mode = TEST_FINISH;
		while(!atTestFinish()){
			ros::spinOnce();
		}
		stop();
		return SUCCESS;
	}

	while(true)
		ros::spin();
	return SUCCESS;	
}

bool Controller::isValidPlay(std::string play){
		for (int i = 0; i < NUM_PLAYS; i++){
			if (play.compare(allowed_plays[i]) == 0){
				mode = i+1;
				return true;	
		}
	}
	return false;
}

/**
* @brief Move to the center of the field
*/
void Controller::goToCenter(){
	us.skillGoToCenter();
}

/**
* @brief Defend the goal
*/
void Controller::defendGoal(){
	us.playDefendGoal();
}

/**
* @brief Try to score a goal
*/
void Controller::rushGoal(){
	us.playRushGoal();
}

/**
* @brief Read in the current game state from a ROS message. 
* The message should have each Object stored in an array with
* the following order:
*  - our robots
*  - opponent's robots
*  - the ball
*
* @param state A pointer to the ROS message. 
*/
void Controller::parseState(const robot_soccer::GameState::ConstPtr& state){
	us.updateState(state);
}

bool Controller::atStartingPosition(){
	return us.atPos(START_POS);

}

bool Controller::atTestStart(){
	return us.atPos(TEST_START_POS);

}

bool Controller::atTestFinish(){
	return us.atPos(TEST_FINISH_POS);

}

void Controller::stop(){
	ros::Rate rate(10);
	for(int i = 0; i < 10; i++){
		us.stop();
		rate.sleep();
	}
}

/*
* @brief Main function for controller node.
*
* @param argc Number of command line arguments
* @param argv Command Line Arguments
*
* @return Exit status
*/
int main(int argc, char **argv){
  return ai.run(argc, argv);
}
