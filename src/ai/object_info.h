/**
* @file ObjectInfo.h
* @brief  
* @author Jonathan Frahm
* @version 1.0
* @date 2015-04-07
*/
#ifndef OBJECT_INFO_H
#define OBJECT_INFO_H
#include "../vision/Vector2.h"

typedef struct {
	Vector location;
	Vector velocity;
	float theta;
	float omega;
	bool on_field;
} ObjectInfo;

#endif
