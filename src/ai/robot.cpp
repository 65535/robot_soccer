#include "robot.h"
#include "controller.h"
#include <iostream>
#include <chrono>
#include <math.h>
#define UNKNOWN -1337.0
#define CLOSE_TO_BALL 0.3
#define CLOSE_TO_OPP 0.3
#define CLOSE_TO_POINT 0.1
#define CLOSE_TO_CORNER 0.15
#define I_MAX 3
#define PI 3.14159
#define MAX_OMEGA 0
#define MAX_SPEED 2 

using namespace ai_node;
namespace {
	float KP = 4;
	float KI = 0;
	float KP_ANG = 3;
	float KI_ANG = 0;
}

float Robot::getFlatSide(float angle){
	float result = angle - PI/2;
	if (result > PI)
		result -= (2*PI);
	return result;
}

/**
* @brief Calculate angular velocity to rotate to face specified direction
*
* @param direction Point to face
*
* @return Angular velocity
*/
float Robot::getAngleToFacePoint(Vector direction){
	static float i = 0;

	float error = atan2(direction.y - self.location.y, direction.x - self.location.x);
	error -= getFlatSide(self.theta);

	float p = KP_ANG * error;
	i += KI_ANG * error;

	if (i > PI && KI_ANG)
		i = (PI - p)/KI_ANG;
	if (i < -PI && KI_ANG)
		i = (-PI - p)/KI_ANG;

	float	adjusted = p + i;

	return adjusted;
}

/**
* @brief Empty constructor for robot
*/
Robot::Robot(){
}

bool Robot::atPos(Vector pos){
	return (Vector::distance(self.location, pos) < CLOSE_TO_POINT);
}

void Robot::stop(){
	self.velocity.x = 0;
	self.velocity.y = 0;
	self.omega = 0;
	publish();
}

/**
* @brief Constructor for robot with ROS topic
*
* @param topic_name Name of the ROS topic to publish mesages to
* @param handle A handle for this ROS node
*/
Robot::Robot(const char *topic_name, ros::NodeHandle* handle){
	publisher = handle->advertise<robot_soccer::Velocity>(topic_name, 5);
}

/**
* @brief Get behind the ball, then push it to the goal
*
* @param ball Location of the ball
*/
void Robot::playRushGoal(){
	aggressiveStateMachine();
}

enum PlayState {
	GET_BEHIND,
	ATTACK
};

void Robot::aggressiveStateMachine(){
	static PlayState state = GET_BEHIND;
	// Calculate target waypoint around opponent robot
	Point waypoint = FieldInfo::GOAL_CENTER;
	if (distanceToLine(opp.location, self.location, waypoint) < CLOSE_TO_OPP
			&& (opp.location.x > self.location.x)){
		Point behind = waypoint - opp.location;
		behind = opp.location - behind.normalized() * CLOSE_TO_OPP;
		Point right = rotate(behind, opp.location, true);
		Point left = rotate(behind, opp.location, false);
		bool right_is_closer = (Vector::distance(self.location, right) <
			(Vector::distance(self.location, left) + 0.2f));
		bool can_go_right = onField(right);
		bool can_go_left = onField(left);

		if (right_is_closer && can_go_right){
			Point dest = right - (self.location - right).normalized() * 2;
			waypoint = dest;
		} else
		if (can_go_left) {
			Point dest = left - (self.location - left).normalized() * 2;
			waypoint = dest;
		}
		std::cout << "Adjusted Waypoint: " << waypoint << std::endl;
		std::cout << "\tRight: " << right << std::endl;
		std::cout << "\tLeft: " << left << std::endl;
	}
	state = (behindBall(waypoint) && okayToApproach(waypoint)) ? ATTACK : GET_BEHIND;
	switch (state) {
		case GET_BEHIND:
			skillGetBehindBall(waypoint);
			if (behindBall(waypoint) && okayToApproach(waypoint)){
				state = ATTACK;
			}
		break;
		case ATTACK:
			skillMoveToPoint(waypoint, waypoint);
			if ((!okayToApproach(waypoint)) || (!behindBall(waypoint))){
				state = GET_BEHIND;
			}
		break;
		default:
			std::cout << "Invalid play state" << std::endl;
			return;
	}
}

bool Robot::checkSide(Point p, Point l1, Point l2){
	return ((((l2.x - l1.x)*(p.y - l1.y)) - ((l2.y - l1.y)*(p.x - l1.x))) > 0);
}

/**
* @brief Simple defend goal play that doesn't require velocity estimation
*
* @param ball location of the ball
*/
void Robot::playDefendGoal(){
	Point goalie_pos = FieldInfo::OUR_GOAL_CENTER;
	goalie_pos.x += CLOSE_TO_BALL;
	goalie_pos.y = ball.location.y;
	if (goalie_pos.y > FieldInfo::GOAL_TOP.y)
		goalie_pos.y = FieldInfo::GOAL_TOP.y;
	if (goalie_pos.y < FieldInfo::GOAL_BOT.y)
		goalie_pos.y = FieldInfo::GOAL_BOT.y;
	skillMoveToPoint(goalie_pos, Point(0, FieldInfo::TOP));
}

/**
* @brief Move the robot to a point on the field
*
* @param location Point to move to
*/
void Robot::skillMoveToPoint(Vector location){
	static Point i(0, 0);
	Point error;

	error.x = location.x - this->self.location.x;
	error.y = location.y - this->self.location.y;

	Point p = KP * error;
	i += KI * error;

	if (i.x > I_MAX && KI)
		i.x = (I_MAX - p.x)/KI;
	if (i.x < -I_MAX && KI)
		i.x = (-I_MAX - p.x)/KI;

	if (i.y > I_MAX && KI)
		i.y = (I_MAX - p.y)/KI;
	if (i.y < -I_MAX && KI)
		i.y = (-I_MAX - p.y)/KI;

	self.velocity = p + i;

	publish();
}

void Robot::skillSpin(){
	Vector v(0,0);
	self.velocity = v;
	self.omega = MAX_OMEGA;
	publish();
}

/**
* @brief Move the robot to a point, while facing a seperate point
*
* @param location Point to move to
* @param direction Point to face
*/
void Robot::skillMoveToPoint(Vector location, Vector direction){
	static Point i(0, 0);
	Point error;
	error.x = location.x - this->self.location.x;
	error.y = location.y - this->self.location.y;

	Point p = KP * error;
	i += KI * error;

	if ((i.x > I_MAX) && KI)
		i.x = (I_MAX - p.x)/KI;
	if ((i.x < -I_MAX) && KI)
		i.x = (-I_MAX - p.x)/KI;

	if ((i.y > I_MAX) && KI)
		i.y = (I_MAX - p.y)/KI;
	if ((i.y < -I_MAX) && KI)
		i.y = (-I_MAX - p.y)/KI;

	self.velocity = p + i;
	self.omega = getAngleToFacePoint(direction);
	if (DEBUG)
		std::cout << "Moving to " << location << " and facing " << direction << std::endl;
	publish();
}

Vector Robot::rotate(Vector p, Vector origin, bool cw){
	Vector v = p;
	v -= origin;
	Vector rotated;
	rotated.x = cw ? v.y : -1*(v.y);
	rotated.y = cw ? -1*(v.x) : v.x;
	rotated += origin;
	return rotated;
}

float Robot::distanceToLine(Point p, Point l1, Point l2){
	float dist = fabs(p.x*(l2.y - l1.y)
				- p.y*(l2.x - l1.x)
				+ l2.x*l1.y - l2.y*l1.x)
					/ sqrt(pow(l2.y - l1.y, 2.0f)
					+ pow(l2.x - l1.x, 2.0f));
	return dist;
}

bool Robot::behindBall(Point waypoint){
	Point behind = waypoint - ball.location;
	behind = ball.location - behind.normalized();
	float margin_of_error = distanceToLine(self.location, behind, ball.location);
	return (margin_of_error < CLOSE_TO_POINT/2);
}

// This function needs to be fixed
bool Robot::okayToApproach(Point waypoint) {
	Point behind = waypoint - ball.location;
	behind = ball.location - behind.normalized() * CLOSE_TO_BALL;
	Point right = rotate(behind, ball.location, true);
	Point left = rotate(behind, ball.location, false);
	return (checkSide(self.location, right, left) != checkSide(waypoint, right, left));
}

bool Robot::onField(Point p ){
	return ((p.y < (FieldInfo::TOP - 0.1)) && (p.y > (FieldInfo::BOTTOM + 0.1))
											&& (p.x < (FieldInfo::RIGHT - 0.1)) && (p.y > (FieldInfo::LEFT + 0.1)));
}

// Returns true if robot is close enough to the point behind the ball
void Robot::skillGetBehindBall(Vector waypoint){
	Point behind = waypoint - ball.location;
	behind = ball.location - behind.normalized() * CLOSE_TO_BALL;
	Point behind_close = ball.location - (waypoint - ball.location).normalized() * CLOSE_TO_POINT;
	bool okay_to_approach = okayToApproach(waypoint);
	if (okay_to_approach){
		if (onField(ball.location))
			skillMoveToPoint(behind, waypoint);
		else
			skillMoveToPoint(behind_close, waypoint);
		return;
	}
	Point right = rotate(behind, ball.location, true);
	Point left = rotate(behind, ball.location, false);
	bool right_is_closer = (Vector::distance(self.location, right) < Vector::distance(self.location, left));
	bool can_go_right = onField(right);
	bool can_go_left = onField(left);
	if (right_is_closer && can_go_right){
		Point dest = right - (self.location - right).normalized()* 2;
		skillMoveToPoint(dest, waypoint);
	} else
	if (can_go_left) {
		Point dest = left - (self.location - left).normalized() * 2;
		skillMoveToPoint(dest, waypoint);
	} else {
		if (Vector::distance(self.location, ball.location) < CLOSE_TO_CORNER)
			skillSpin();
		else
			skillMoveToPoint(behind_close, waypoint);
	}
}

/**
* @brief Move the robot to the center of the field
*/
void Robot::skillGoToCenter(){
	skillMoveToPoint(FieldInfo::CENTER, FieldInfo::GOAL_CENTER);
}

/**
* @brief Update member variables based on message from vision node
*
* @param msg Object messgage received from the vision node
*/
void Robot::updateState(const robot_soccer::GameState::ConstPtr& state){
	unsigned char counter = 0;
	// Update state info for robot
	robot_soccer::Object msg = state->objects[counter++];
	updateState(&self, msg);
	// Update state info for opponent
	msg = state->objects[counter++];
	updateState(&opp, msg);
	// Update state info for ball
	msg = state->objects[counter++];
	updateState(&ball, msg);

}

void Robot::updateState(ObjectInfo* obj, robot_soccer::Object msg){
	if (msg.x == UNKNOWN && msg.y == UNKNOWN){
    obj->on_field = false;
  } else {
    obj->on_field = true;
  }
  obj->location.x = (msg.x==UNKNOWN) ? 0 : msg.x;
  obj->location.y = (msg.y==UNKNOWN) ? 0 : msg.y;
  obj->theta = (msg.angle==UNKNOWN) ? 0 : msg.angle;
	if (msg.x == UNKNOWN && msg.y == UNKNOWN){
		obj->on_field = false;
	} else {
		obj->on_field = true;
	}
	obj->location.x = (msg.x==UNKNOWN) ? 0 : msg.x;
	obj->location.y = (msg.y==UNKNOWN) ? 0 : msg.y;
	obj->theta = (msg.angle==UNKNOWN) ? 0 : msg.angle;
}

/**
* @brief Publish to the ROS topic for this robot
*/
void Robot::publish(){
	if (!self.on_field)
		return;
	if(self.velocity.length() > MAX_SPEED){
		self.velocity = MAX_SPEED * self.velocity.normalized();
	}
	if(self.omega > MAX_OMEGA){
		self.omega = MAX_OMEGA;
	} else if (self.omega < -MAX_OMEGA) {
		self.omega = -MAX_OMEGA;
	}
	robot_soccer::Velocity v;
	v.x = self.velocity.x;
	v.y = self.velocity.y;
	v.omega = self.omega;
	v.theta = self.theta;
	if (DEBUG){
		std::cout << "--Publishing Velocity--" << std::endl;
		std::cout << "\tx: " << v.x << std::endl;
		std::cout << "\ty: " << v.y << std::endl;
		std::cout << "\ttheta: " << v.theta << std::endl;
		std::cout << "\tomega: " << v.omega << std::endl << std::endl;
	}
	publisher.publish(v);
}


