/**
* @file controller.h
* @brief Header file for AI Controller
*
* @author Jonathan Frahm
* @version 1.0
* @date 2015-02-01
*/
#ifndef CONTROLLER_H
#define CONTROLLER_H
#define ROBOTS_PER_TEAM 2

#include "ros/ros.h"

// msg declarations
#include "robot_soccer/GameState.h"
#include "robot_soccer/Object.h"
#include "robot_soccer/Velocity.h"
#include "../vision/Vector2.h"
#include "robot.h"
#include <string>

namespace ai_node {
  /**
  * @brief Print debug information
  */
  extern bool DEBUG;
}

/**
* @brief Runs the main AI for the 
* robot soccer team.
*/
class Controller {
public:
	int run(int argc, char **argv);
	void goToCenter();
	void rushGoal();
	void defendGoal();
	void moveInBlock();
	void parseState(const robot_soccer::GameState::ConstPtr& state);
	void processGameState(const robot_soccer::GameState::ConstPtr& state);
	bool atStartingPosition();
	bool atTestStart();
	bool atTestFinish();
	void stop();

private:
	bool isValidPlay(std::string play);
	/**
	* @brief Models for robots on our team
	*/
	Robot us;
	/**
	* @brief Models for robots on the opponent's team
	*/
	Robot them;
	/**
	* @brief Location of the ball
	*/
	Vector ball;
	/**
	* @brief Estimated velocity of the ball
	*/
	Vector v_ball;

	/**
	* @brief Subscriber to receive messages about the game state from the vision node
	*/
	ros::Subscriber gameState;
}; 

#endif
