/**
* @file robot.cpp
* @brief Function definitions for the Robot class.
* This file also contains the main function for this
* ROS node. The node subscribes to a ROS topic based
* on command line input. Each robot should subscribe
* a unique topic.
*
* @author Jonathan Frahm
* @version 1.0
* @date 2015-02-01
*/
#include "boost/program_options.hpp"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <assert.h>
#include "ros/ros.h"
#include "robot_soccer/Velocity.h"

namespace {
	/**
	* @brief Print debug information
	*/
	bool DEBUG = false;
	/**
	* @brief
	*/
	int BAUD_RATE = 115200;
	std::string USB_PIPE = "/dev/ttyACM0";
	std::string VELOCITY_TOPIC = "robot1";
	float DIR_SCALAR = 5;
	float MAX_VEL = 10;
	float ANG_SCALAR = 2;
	float MAX_ANG = 20;
	int MSG_SIZE = 2;
	std::ofstream psoc;

	//Exit codes
	const size_t SUCCESS = 0;
	const size_t ERROR_IN_COMMAND_LINE = 1;
}


/**
* @brief Handler for subscription to velocity commands
*
* @param vel Message received
*/
void processVelocity(const robot_soccer::Velocity::ConstPtr& vel){
	float dir_msg[MSG_SIZE];
	float ang_msg[MSG_SIZE];
	char header = 'v';
	dir_msg[0] = vel->x * DIR_SCALAR;
	dir_msg[1] = vel->y * DIR_SCALAR;
	ang_msg[0] = vel->omega * ANG_SCALAR;
	ang_msg[1] = vel->theta;
	char footer = '0';

	// Limit linear velocities
	if (dir_msg[0] > MAX_VEL)
		dir_msg[0] = MAX_VEL;
	else if (dir_msg[0] < -MAX_VEL)
		dir_msg[0] = -MAX_VEL;
	if (dir_msg[1] > MAX_VEL)
		dir_msg[1] = MAX_VEL;
	else if (dir_msg[1] < -MAX_VEL)
		dir_msg[1] = -MAX_VEL;

	// Limit angular velocity
	if (ang_msg[0] > MAX_ANG)
		ang_msg[0] = MAX_ANG;
	else if (ang_msg[0] < -MAX_ANG)
		ang_msg[0] = -MAX_ANG;

	if(DEBUG){
		std::cout << "x: " << dir_msg[0] << std::endl;
		std::cout << "y: " << dir_msg[1] << std::endl;
		std::cout << "omega: " << ang_msg[0] << std::endl;
		std::cout << "theta: " << ang_msg[1] << std::endl;
		assert(CHAR_BIT * sizeof (float) == 32);
	}

	psoc.write(&header, sizeof(char));
	psoc.write((char*)dir_msg, sizeof(dir_msg));
	psoc.write((char*)ang_msg, sizeof(ang_msg));
	psoc.write(&footer, sizeof(char));
	psoc.flush();
	return;
}


/**
* @brief Top level control for the velocity processor.
*
* @param argc Number of command line arguments
* @param argv Command line arguments. The second argument
* should be the name of the ROS topic the robot subscribes
* to for velocity commands.
*/
void play(){
	// This object is the main access point to communications with
	// the ROS system
	ros::NodeHandle handle;

	psoc.open(USB_PIPE.c_str(), std::ios::binary|std::ios::out);
	// Initialize a topic for communicating velocities to
	// the robots
	ros::Subscriber robot = handle.subscribe(VELOCITY_TOPIC, 5, processVelocity);
	// ros::Publisher robot1 = handle.subscribe(topic, 5);

	// Keep alive until the node is shutdown
	ros::spin();

	return;
}

/**
* @brief Main function for the robot node.
*
* @param argc Number of command line  arguments
* @param argv Command line arguments. The second argument
* should be the name of the topic the node is subscribing to
* for velocity commands.
*
* @return exit status
*/
int main(int argc, char **argv){
	std::string target = "robot1";
  namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()
    ("help,h", "Print help messages")
    ("baud-rate,b",po::value<int>(&BAUD_RATE),"baud rate for USB connection")
    ("linear-scalar,l",po::value<float>(&DIR_SCALAR),"scalar to convert from m/s to PSoC's units")
    ("angular-scalar,a",po::value<float>(&ANG_SCALAR),"scalar to convert from radians/s to PSoC's units")
    ("max-vel",po::value<float>(&MAX_VEL),"maximum linear velocity to send to the PSoC")
    ("max-ang",po::value<float>(&MAX_ANG),"maximum omega to send to the PSoC")
    ("topic",po::value<std::string>(&VELOCITY_TOPIC),"ROS topic this node should subscribe to")
    ("tty-filename",po::value<std::string>(&USB_PIPE),"filename of the pipe for usb communications")
    ("debug,d", "print debug information");

  po::variables_map vm;
  try{
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")){
      std::cout << "Velocity processor node" << std::endl
								<< "Usage: rosrun robot_soccer velocity_processor [options]" << std::endl
                << desc << std::endl;
      return SUCCESS;
    }
		DEBUG = vm.count("debug");
  }
  catch(po::error& e){
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl
              << desc << std::endl;
    return ERROR_IN_COMMAND_LINE;
  }

	// Initialize 'controller' node with ROS
	ros::init(argc, argv, "robot");

	play();
}
